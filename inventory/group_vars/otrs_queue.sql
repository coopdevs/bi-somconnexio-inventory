drop view v_otrs_queues_history ;
create view v_otrs_queues_history as
select th.ticket_id, min(th.create_time) as create_time , max(th.create_time ) as max_create_time
,date_part('day', max(th.create_time) -min(th.create_time))*24+date_part('hour', max(th.create_time) -min(th.create_time))+date_part('minute', max(th.create_time) -min(th.create_time))/60.0
as hours_in_queue
,date_part('day', max(th.create_time) -min(th.create_time))+date_part('hour', max(th.create_time) -min(th.create_time))/24.0
as days_in_queue
,q.name as queue_name, gt.name as group_name, gt.comments as group_comments
,tt.name as ticket_type -- , tt.id
, dfv.value_text as tecnologia, pon.value_text as pon
from otrs_ticket_history th
	join otrs_ticket_history_type tht on th.history_type_id =tht.id
	join otrs_ticket t on th.ticket_id =t.id
	join otrs_ticket_type tt on tt.id=t.type_id
	join otrs_queue q on th.queue_id =q.id
	join otrs_groups_table gt on gt.id=q.group_id
	left join (
		select dfv.object_id , dfv.value_text
		from otrs_dynamic_field_value dfv join otrs_dynamic_field df on dfv.field_id =df.id
		where df.name ='TecDelServei'
		) dfv on dfv.object_id =t.id
	left join (
		select dfv.object_id , dfv.value_text
		from otrs_dynamic_field_value dfv join otrs_dynamic_field df on dfv.field_id =df.id
		where df.name ='pon'
		) pon on pon.object_id =t.id
where 1=1
group by th.ticket_id
,q.name, gt.name, gt.comments
,tt.name
,dfv.value_text, pon.value_text;





create table external.otrs_queue_hierarchy(
	id int,
	name varchar(200),
	process varchar(200),
	provider varchar(200)
);

insert into external.otrs_queue_hierarchy values(86,'Bústia Serveis','','');
insert into external.otrs_queue_hierarchy values(87,'Bústia Serveis::ADSL','','');
insert into external.otrs_queue_hierarchy values(88,'Bústia Serveis::ADSL::ADSL N1','','');
insert into external.otrs_queue_hierarchy values(90,'Bústia Serveis::ADSL::ADSL N1::Incidència','','');
insert into external.otrs_queue_hierarchy values(108,'Bústia Serveis::ADSL::ADSL N1::Petició','','');
insert into external.otrs_queue_hierarchy values(89,'Bústia Serveis::ADSL::ADSL N2','','');
insert into external.otrs_queue_hierarchy values(109,'Bústia Serveis::ADSL::ADSL N2::Incidència','','');
insert into external.otrs_queue_hierarchy values(110,'Bústia Serveis::ADSL::ADSL N2::Petició','','');
insert into external.otrs_queue_hierarchy values(4,'Bústia Serveis::Facturació','','');
insert into external.otrs_queue_hierarchy values(5,'Bústia Serveis::Facturació::Nivel 1','','');
insert into external.otrs_queue_hierarchy values(6,'Bústia Serveis::Facturació::Nivel 2','','');
insert into external.otrs_queue_hierarchy values(111,'Bústia Serveis::Fibra MM','','');
insert into external.otrs_queue_hierarchy values(112,'Bústia Serveis::Fibra MM::Fibra MM N1','','');
insert into external.otrs_queue_hierarchy values(114,'Bústia Serveis::Fibra MM::Fibra MM N1::Incidència','','');
insert into external.otrs_queue_hierarchy values(115,'Bústia Serveis::Fibra MM::Fibra MM N1::Petició','','');
insert into external.otrs_queue_hierarchy values(113,'Bústia Serveis::Fibra MM::Fibra MM N2','','');
insert into external.otrs_queue_hierarchy values(116,'Bústia Serveis::Fibra MM::Fibra MM N2::Incidència','','');
insert into external.otrs_queue_hierarchy values(120,'Bústia Serveis::Fibra MM::Fibra MM N2::Petició','','');
insert into external.otrs_queue_hierarchy values(117,'Bústia Serveis::Fibra VDF','','');
insert into external.otrs_queue_hierarchy values(118,'Bústia Serveis::Fibra VDF::Fibra VDF N1','','');
insert into external.otrs_queue_hierarchy values(121,'Bústia Serveis::Fibra VDF::Fibra VDF N1::Incidència','','');
insert into external.otrs_queue_hierarchy values(122,'Bústia Serveis::Fibra VDF::Fibra VDF N1::Petició','','');
insert into external.otrs_queue_hierarchy values(119,'Bústia Serveis::Fibra VDF::Fibra VDF N2','','');
insert into external.otrs_queue_hierarchy values(123,'Bústia Serveis::Fibra VDF::Fibra VDF N2::Incidència','','');
insert into external.otrs_queue_hierarchy values(124,'Bústia Serveis::Fibra VDF::Fibra VDF N2::Petició','','');
insert into external.otrs_queue_hierarchy values(125,'Bústia Serveis::Mòbil','','');
insert into external.otrs_queue_hierarchy values(126,'Bústia Serveis::Mòbil::Mòbil N1','','');
insert into external.otrs_queue_hierarchy values(128,'Bústia Serveis::Mòbil::Mòbil N1::Incidència','','');
insert into external.otrs_queue_hierarchy values(129,'Bústia Serveis::Mòbil::Mòbil N1::Petició','','');
insert into external.otrs_queue_hierarchy values(127,'Bústia Serveis::Mòbil::Mòbil N2','','');
insert into external.otrs_queue_hierarchy values(130,'Bústia Serveis::Mòbil::Mòbil N2::Incidència','','');
insert into external.otrs_queue_hierarchy values(131,'Bústia Serveis::Mòbil::Mòbil N2::Petició','','');
insert into external.otrs_queue_hierarchy values(2,'Info','','');
insert into external.otrs_queue_hierarchy values(93,'Oficina Virtual','','');
insert into external.otrs_queue_hierarchy values(106,'Oficina Virtual::Canvi email compte','','');
insert into external.otrs_queue_hierarchy values(100,'Oficina Virtual::Canvi email contractes','','');
insert into external.otrs_queue_hierarchy values(94,'Oficina Virtual::Canvi IBAN contractes','','');
insert into external.otrs_queue_hierarchy values(95,'Oficina Virtual::Canvi Tarifa mòbil','','');
insert into external.otrs_queue_hierarchy values(98,'Oficina Virtual::Canvi Tarifa mòbil::canvi KO','','');
insert into external.otrs_queue_hierarchy values(99,'Oficina Virtual::Canvi Tarifa mòbil::canvi OK','','');
insert into external.otrs_queue_hierarchy values(96,'Oficina Virtual::Canvi Tarifa mòbil::Enviat a MM','','');
insert into external.otrs_queue_hierarchy values(132,'Oficina Virtual::Canvi Tarifa mòbil::Massius','','');
insert into external.otrs_queue_hierarchy values(97,'Oficina Virtual::Canvi Tarifa mòbil::Rebut','','');
insert into external.otrs_queue_hierarchy values(101,'Oficina Virtual::Dades addicionals','','');
insert into external.otrs_queue_hierarchy values(102,'Oficina Virtual::Dades addicionals::Dades KO','','');
insert into external.otrs_queue_hierarchy values(103,'Oficina Virtual::Dades addicionals::Dades OK','','');
insert into external.otrs_queue_hierarchy values(104,'Oficina Virtual::Dades addicionals::Enviat a MM (Dades)','','');
insert into external.otrs_queue_hierarchy values(105,'Oficina Virtual::Dades addicionals::Rebut Dades','','');
insert into external.otrs_queue_hierarchy values(16,'Provisió ADSL','','');
insert into external.otrs_queue_hierarchy values(47,'Revisió OpenCell','','');
insert into external.otrs_queue_hierarchy values(11,'Serveis de banda ampla','','');
insert into external.otrs_queue_hierarchy values(57,'Serveis de banda ampla::Baixa servei BA','','');
insert into external.otrs_queue_hierarchy values(58,'Serveis de banda ampla::Baixa servei BA::Baixa ADSL','','');
insert into external.otrs_queue_hierarchy values(59,'Serveis de banda ampla::Baixa servei BA::Baixa ADSL::Baixa finalitzada','','');
insert into external.otrs_queue_hierarchy values(60,'Serveis de banda ampla::Baixa servei BA::Baixa ADSL::Baixa sol·licitada','','');
insert into external.otrs_queue_hierarchy values(61,'Serveis de banda ampla::Baixa servei BA::Baixa ADSL::Pendent de fer baixa','','');
insert into external.otrs_queue_hierarchy values(62,'Serveis de banda ampla::Baixa servei BA::Baixa MM','','');
insert into external.otrs_queue_hierarchy values(63,'Serveis de banda ampla::Baixa servei BA::Baixa MM::Baixa finalitzada','','');
insert into external.otrs_queue_hierarchy values(64,'Serveis de banda ampla::Baixa servei BA::Baixa MM::Baixa sol·licitada','','');
insert into external.otrs_queue_hierarchy values(65,'Serveis de banda ampla::Baixa servei BA::Baixa MM::Pendent de fer baixa','','');
insert into external.otrs_queue_hierarchy values(66,'Serveis de banda ampla::Baixa servei BA::Baixa Vdf','','');
insert into external.otrs_queue_hierarchy values(67,'Serveis de banda ampla::Baixa servei BA::Baixa Vdf::Baixa finalitzada','','');
insert into external.otrs_queue_hierarchy values(68,'Serveis de banda ampla::Baixa servei BA::Baixa Vdf::Baixa sol·licitada','','');
insert into external.otrs_queue_hierarchy values(69,'Serveis de banda ampla::Baixa servei BA::Baixa Vdf::Pendent de fer baixa','','');
insert into external.otrs_queue_hierarchy values(42,'Serveis de banda ampla::Consulta de cobertura CAT','','');
insert into external.otrs_queue_hierarchy values(43,'Serveis de banda ampla::Consulta de cobertura ES','','');
insert into external.otrs_queue_hierarchy values(15,'Serveis de banda ampla::Incidencia Proveedor','','');
insert into external.otrs_queue_hierarchy values(12,'Serveis de banda ampla::Nivel 1','','');
insert into external.otrs_queue_hierarchy values(17,'Serveis de banda ampla::Provisió ADSL','Provisió ADSL','Jazztel');
insert into external.otrs_queue_hierarchy values(18,'Serveis de banda ampla::Provisió ADSL::01. Esperar documentació','Provisió ADSL','Jazztel');
insert into external.otrs_queue_hierarchy values(19,'Serveis de banda ampla::Provisió ADSL::02. Router pendent d''enviar','Provisió ADSL','Jazztel');
insert into external.otrs_queue_hierarchy values(20,'Serveis de banda ampla::Provisió ADSL::03. Router enviat','Provisió ADSL','Jazztel');
insert into external.otrs_queue_hierarchy values(21,'Serveis de banda ampla::Provisió ADSL::04. Realitzar comandes ADSL','Provisió ADSL','Jazztel');
insert into external.otrs_queue_hierarchy values(30,'Serveis de banda ampla::Provisió ADSL::05. Comandes ADSL pendents de finalitzar','Provisió ADSL','Jazztel');
insert into external.otrs_queue_hierarchy values(23,'Serveis de banda ampla::Provisió ADSL::06. Portabilitat fix pendents','Provisió ADSL','Jazztel');
insert into external.otrs_queue_hierarchy values(25,'Serveis de banda ampla::Provisió ADSL::07. Dades endpoint','Provisió ADSL','Jazztel');
insert into external.otrs_queue_hierarchy values(26,'Serveis de banda ampla::Provisió ADSL::08. Configurar endpoint','Provisió ADSL','Jazztel');
insert into external.otrs_queue_hierarchy values(27,'Serveis de banda ampla::Provisió ADSL::09. Pendents de fer baixa','Provisió ADSL','Jazztel');
insert into external.otrs_queue_hierarchy values(28,'Serveis de banda ampla::Provisió ADSL::10. Començar facturació','Provisió ADSL','Jazztel');
insert into external.otrs_queue_hierarchy values(29,'Serveis de banda ampla::Provisió ADSL::11. Contractes ADSL creats','Provisió ADSL','Jazztel');
insert into external.otrs_queue_hierarchy values(41,'Serveis de banda ampla::Provisió ADSL::12. Provisió ADSL abandonada','Provisió ADSL','Jazztel');
insert into external.otrs_queue_hierarchy values(22,'Serveis de banda ampla::Provisió ADSL::Altes ADSL finalitzades','Provisió ADSL','Jazztel');
insert into external.otrs_queue_hierarchy values(40,'Serveis de banda ampla::Provisió ADSL::Altes ADSL pendents','','');
insert into external.otrs_queue_hierarchy values(24,'Serveis de banda ampla::Provisió ADSL::Altes fix finalitzades','','');
insert into external.otrs_queue_hierarchy values(31,'Serveis de banda ampla::Provisió Fibra','','');
insert into external.otrs_queue_hierarchy values(136,'Serveis de banda ampla::Provisió Fibra::1. Provisió VDF','Provisió Fibra','Vodafone');
insert into external.otrs_queue_hierarchy values(32,'Serveis de banda ampla::Provisió Fibra::1. Provisió VDF::01. Confirmar documentació VDF','Provisió Fibra','Vodafone');
insert into external.otrs_queue_hierarchy values(33,'Serveis de banda ampla::Provisió Fibra::1. Provisió VDF::02. Pendents de provisió (Vodafone)','Provisió Fibra','Vodafone');
insert into external.otrs_queue_hierarchy values(46,'Serveis de banda ampla::Provisió Fibra::1. Provisió VDF::03. Canvis d''ubicació interns','Provisió Fibra','Vodafone');
insert into external.otrs_queue_hierarchy values(70,'Serveis de banda ampla::Provisió Fibra::1. Provisió VDF::03. Canvis d''ubicació mateix titular','Provisió Fibra','Vodafone');
insert into external.otrs_queue_hierarchy values(34,'Serveis de banda ampla::Provisió Fibra::1. Provisió VDF::03. Pendents de finalitzar','Provisió Fibra','Vodafone');
insert into external.otrs_queue_hierarchy values(56,'Serveis de banda ampla::Provisió Fibra::1. Provisió VDF::03. Pendents de tramitar','Provisió Fibra','Vodafone');
insert into external.otrs_queue_hierarchy values(39,'Serveis de banda ampla::Provisió Fibra::1. Provisió VDF::04. Cancel·lades pendents de rellençar Vdf','Provisió Fibra','Vodafone');
insert into external.otrs_queue_hierarchy values(36,'Serveis de banda ampla::Provisió Fibra::1. Provisió VDF::04. K.O.s','Provisió Fibra','Vodafone');
insert into external.otrs_queue_hierarchy values(38,'Serveis de banda ampla::Provisió Fibra::1. Provisió VDF::04. K.O.s definitius','Provisió Fibra','Vodafone');
insert into external.otrs_queue_hierarchy values(54,'Serveis de banda ampla::Provisió Fibra::1. Provisió VDF::04. KOs rellançats','Provisió Fibra','Vodafone');
insert into external.otrs_queue_hierarchy values(35,'Serveis de banda ampla::Provisió Fibra::1. Provisió VDF::05. Finalitzar comanda fibra','Provisió Fibra','Vodafone');
insert into external.otrs_queue_hierarchy values(37,'Serveis de banda ampla::Provisió Fibra::1. Provisió VDF::06. Contractes fibra creats','Provisió Fibra','Vodafone');
insert into external.otrs_queue_hierarchy values(44,'Serveis de banda ampla::Provisió Fibra::1. Provisió VDF::07. Provisió fibra Vdf pausada','Provisió Fibra','Vodafone');
insert into external.otrs_queue_hierarchy values(78,'Serveis de banda ampla::Provisió Fibra::1. Provisió VDF::08. Provisió Fibra abandonada Vdf','Provisió Fibra','Vodafone');
insert into external.otrs_queue_hierarchy values(48,'Serveis de banda ampla::Provisió Fibra::1. Provisió VDF::09. Incidència provisió Fibra','Provisió Fibra','Vodafone');
insert into external.otrs_queue_hierarchy values(135,'Serveis de banda ampla::Provisió Fibra::2. Provisió MM','Provisió Fibra','MásMóvil');
insert into external.otrs_queue_hierarchy values(49,'Serveis de banda ampla::Provisió Fibra::2. Provisió MM::1. Confirmar documentació MM','Provisió Fibra','MásMóvil');
insert into external.otrs_queue_hierarchy values(50,'Serveis de banda ampla::Provisió Fibra::2. Provisió MM::2. Realitzar comanda Fibra MM','Provisió Fibra','MásMóvil');
insert into external.otrs_queue_hierarchy values(51,'Serveis de banda ampla::Provisió Fibra::2. Provisió MM::3. Pendent de finalitzar provisió MM','Provisió Fibra','MásMóvil');
insert into external.otrs_queue_hierarchy values(52,'Serveis de banda ampla::Provisió Fibra::2. Provisió MM::4. Incidència provisió MM','Provisió Fibra','MásMóvil');
insert into external.otrs_queue_hierarchy values(55,'Serveis de banda ampla::Provisió Fibra::2. Provisió MM::5. Provisió fibra MM pausada','Provisió Fibra','MásMóvil');
insert into external.otrs_queue_hierarchy values(53,'Serveis de banda ampla::Provisió Fibra::2. Provisió MM::6. Contractes Fibra MM creats','Provisió Fibra','MásMóvil');
insert into external.otrs_queue_hierarchy values(45,'Serveis de banda ampla::Provisió Fibra::2. Provisió MM::7. Provisió Fibra abandonada MM','Provisió Fibra','MásMóvil');
insert into external.otrs_queue_hierarchy values(134,'Serveis de banda ampla::Provisió Fibra::3. Provisió XOLN','Provisió Fibra','XOLN');
insert into external.otrs_queue_hierarchy values(84,'Serveis de banda ampla::Provisió Fibra::3. Provisió XOLN::1. XOLN','Provisió Fibra','XOLN');
insert into external.otrs_queue_hierarchy values(91,'Serveis de banda ampla::Provisió Fibra::3. Provisió XOLN::2. Pendents finalitzar XOLN','Provisió Fibra','XOLN');
insert into external.otrs_queue_hierarchy values(92,'Serveis de banda ampla::Provisió Fibra::3. Provisió XOLN::3. Contractes XOLN creats','Provisió Fibra','XOLN');
insert into external.otrs_queue_hierarchy values(137,'Serveis de banda ampla::Provisió Fibra::Provisió Orange','Provisió Fibra Orange Sense Fix','Orange');
insert into external.otrs_queue_hierarchy values(138,'Serveis de banda ampla::Provisió Fibra::Provisió Orange::01. Confirmar documentació Orange','Provisió Fibra Orange Sense Fix','Orange');
insert into external.otrs_queue_hierarchy values(139,'Serveis de banda ampla::Provisió Fibra::Provisió Orange::02. Realitzar comanda Fibra Orange','Provisió Fibra Orange Sense Fix','Orange');
insert into external.otrs_queue_hierarchy values(140,'Serveis de banda ampla::Provisió Fibra::Provisió Orange::03. Pendent de finalitzar provisió Orange','Provisió Fibra Orange Sense Fix','Orange');
insert into external.otrs_queue_hierarchy values(141,'Serveis de banda ampla::Provisió Fibra::Provisió Orange::04. Incidència provisió ORG','Provisió Fibra Orange Sense Fix','Orange');
insert into external.otrs_queue_hierarchy values(142,'Serveis de banda ampla::Provisió Fibra::Provisió Orange::05. Contracte ORG creat','Provisió Fibra Orange Sense Fix','Orange');
insert into external.otrs_queue_hierarchy values(146,'Serveis de banda ampla::Provisió Router 4G','Provisió Router 4G','Vodafone');
insert into external.otrs_queue_hierarchy values(147,'Serveis de banda ampla::Provisió Router 4G::1. Pendent confirmar documentació','Provisió Router 4G','Vodafone');
insert into external.otrs_queue_hierarchy values(148,'Serveis de banda ampla::Provisió Router 4G::2. Pendent provisió','Provisió Router 4G','Vodafone');
insert into external.otrs_queue_hierarchy values(149,'Serveis de banda ampla::Provisió Router 4G::3. Pendent finalitzar','Provisió Router 4G','Vodafone');
insert into external.otrs_queue_hierarchy values(150,'Serveis de banda ampla::Provisió Router 4G::4. Incidència provisió 4G','Provisió Router 4G','Vodafone');
insert into external.otrs_queue_hierarchy values(151,'Serveis de banda ampla::Provisió Router 4G::5. Contracte 4G creat','Provisió Router 4G','Vodafone');
insert into external.otrs_queue_hierarchy values(13,'Serveis de banda ampla::Resolución no inmediata','','');
insert into external.otrs_queue_hierarchy values(14,'Serveis de banda ampla::Técnico nivel 2','','');
insert into external.otrs_queue_hierarchy values(7,'Serveis mòbil','','');
insert into external.otrs_queue_hierarchy values(10,'Serveis mòbil::Incidencia Proveedor','','');
insert into external.otrs_queue_hierarchy values(8,'Serveis mòbil::Nivel 1','','');
insert into external.otrs_queue_hierarchy values(71,'Serveis mòbil::Provisió mòbil','Provisió Mòbil','MásMóvil');
insert into external.otrs_queue_hierarchy values(72,'Serveis mòbil::Provisió mòbil::01. Sol·licitat','Provisió Mòbil','MásMóvil');
insert into external.otrs_queue_hierarchy values(73,'Serveis mòbil::Provisió mòbil::02. Activació programada','Provisió Mòbil','MásMóvil');
insert into external.otrs_queue_hierarchy values(74,'Serveis mòbil::Provisió mòbil::03. Error petició','Provisió Mòbil','MásMóvil');
insert into external.otrs_queue_hierarchy values(75,'Serveis mòbil::Provisió mòbil::04. Introduït a la plataforma','Provisió Mòbil','MásMóvil');
insert into external.otrs_queue_hierarchy values(76,'Serveis mòbil::Provisió mòbil::05. Error d''activació (Rebutjades)','Provisió Mòbil','MásMóvil');
insert into external.otrs_queue_hierarchy values(77,'Serveis mòbil::Provisió mòbil::06. Contractes mòbil creats','Provisió Mòbil','MásMóvil');
insert into external.otrs_queue_hierarchy values(9,'Serveis mòbil::Resolución no inmediata','','');
insert into external.otrs_queue_hierarchy values(1,'Servicios','','');
insert into external.otrs_queue_hierarchy values(79,'Sistemes','','');
insert into external.otrs_queue_hierarchy values(85,'Sistemes::Altres','','');
insert into external.otrs_queue_hierarchy values(143,'Sistemes::Error formularis','','');
insert into external.otrs_queue_hierarchy values(80,'Sistemes::Formularis','','');
insert into external.otrs_queue_hierarchy values(81,'Sistemes::Odoo','','');
insert into external.otrs_queue_hierarchy values(83,'Sistemes::Opencell','','');
insert into external.otrs_queue_hierarchy values(82,'Sistemes::OTRS','','');
insert into external.otrs_queue_hierarchy values(107,'Sistemes::OV','','');
insert into external.otrs_queue_hierarchy values(133,'Sistemes::Split','','');
insert into external.otrs_queue_hierarchy values(3,'Spam','','');
insert into external.otrs_queue_hierarchy values(144,'Vols que et truquem? (CAT)','','');
insert into external.otrs_queue_hierarchy values(145,'Vols que et truquem? (ES)','','');
