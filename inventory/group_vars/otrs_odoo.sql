create or replace view v_detall_factures_opencell as
select d.code, d.invoice_date, d.invoice_number, d.amount_without_tax_invoice , d.amount_without_tax_rated_transaction , d.code_rated_transaction, d.quantity_rated_transaction,
c.id, c.message_main_attachment_id, c.active, c.group_id, c.manual_currency_id, c.contract_template_id, c.user_id, c.recurring_next_date, c.date_end, c.payment_term_id, c.fiscal_position_id, c.invoice_partner_id, c.partner_id, c.commercial_partner_id, c.note, c.is_terminated, c.terminate_reason_id, c.terminate_comment, c.terminate_date, c."name", c.pricelist_id, c.contract_type, c.journal_id, c.company_id, c.create_uid, c.create_date, c.write_uid, c.write_date, c.payment_mode_id, c.mandate_id, c.service_technology_id, c.service_supplier_id, c.service_partner_id, c.crm_lead_line_id, c.mobile_contract_service_info_id, c.vodafone_fiber_service_contract_info_id, c.mm_fiber_service_contract_info_id, c.adsl_service_contract_info_id, c.date_start, c.phone_number, c.ticket_number, c.terminate_user_reason_id, c.xoln_fiber_service_contract_info_id,
st.name as technology_name, ss.name as supplier_name, p.default_code as producte, c.date_start as date_start_contract, c.date_end as date_end_contract
from external.detall_factures_opencell d
  join odoo_contract_contract c on d.code=c.code
  join odoo_service_technology st on c.service_technology_id =st.id
  join odoo_service_supplier ss on c.service_supplier_id = ss.id
  join odoo_product_product p on p.id=c.current_tariff_product;


create or replace view  v_detall_factures_opencell_month as
  select d.data, o.code, o.amount_without_tax_rated_transaction, o.supplier_name, o.technology_name, o.producte
  from v_detall_factures_opencell o
  	join (
 		select data
 		from "data" d
 		where date_part('day',data)=1
 	) d on date_trunc('month', o.invoice_date)=d.data
 			and date_trunc('month', o.date_start_contract)<>d.data
 			and date_trunc('month', o.date_end_contract)<>d.data;
