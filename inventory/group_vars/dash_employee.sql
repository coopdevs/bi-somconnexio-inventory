
drop view if exists employee_calendar;
create view employee_calendar as
select distinct theoretical_hours_start_date
,hd.name as department_name, hd.complete_name as department_complete_name
, rc.name as resource_calendar_name, rc.hours_per_day
, orca.dayofweek
,he.id, he.name
 from odoo_hr_employee he
join odoo_hr_department hd on hd.id=he.department_id
join odoo_resource_calendar rc on he.resource_calendar_id =rc.id
join odoo_resource_calendar_attendance orca on orca.calendar_id =rc.id ;


create view employee_leaves as
select he.id, hl.request_date_from, hl.request_date_to , number_of_days
from odoo_hr_leave hl
join odoo_hr_employee he on hl.employee_id =he.id;
