DROP TABLE IF EXISTS data CASCADE;

create table data( data date, dies_mes smallint);
insert into data (data)
select  cast('2009-12-31' as date) + ((rn||' days')::INTERVAL)
from (
select row_number() over (order by (select 1)) as rn from odoo_contract_contract limit 5000
) a;

 update data set dies_mes=
    DATE_PART('days',
        DATE_TRUNC('month', data.data)
        + '1 MONTH'::INTERVAL
        - '1 DAY'::INTERVAL
    );


DROP VIEW IF EXISTS odoo_altes_socia CASCADE;
CREATE VIEW odoo_altes_socia AS
SELECT data, SUM(i.amount)/dies_mes AS altes_socia
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 677
 and date_FROM <='20201231'
GROUP BY data, dies_mes
UNION ALL
SELECT CAST(s.create_date AS date) AS data, count(*) AS altes_socia
FROM odoo_subscription_request s
	LEFT JOIN odoo_res_partner p ON s.partner_id = p.id
WHERE (s.type = 'new' OR s.type = 'subscription')
  AND (s.state = 'done' OR s.state = 'draft' OR s.state = 'paid')
  and s.create_date>='20210101'
GROUP BY CAST(s.create_date AS date);

DROP VIEW IF EXISTS odoo_baixes_socia CASCADE;
CREATE VIEW odoo_baixes_socia AS
SELECT data, SUM(i.amount)/dies_mes AS baixes_socia
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 227
 and date_FROM <='20201231'
GROUP BY data, dies_mes
UNION ALL
SELECT CAST(effective_date AS date), count(*) AS count
FROM odoo_operation_request r
WHERE r.operation_type = 'sell_back' and effective_date>='20210101'
GROUP BY CAST(effective_date AS date);

DROP VIEW IF EXISTS odoo_altes_netes_socia CASCADE;
CREATE VIEW odoo_altes_netes_socia AS
SELECT data, SUM(i.amount)/dies_mes AS altes_netes_socia
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 677
 and date_FROM <='20201231'
GROUP BY data, dies_mes
UNION ALL
SELECT
CAST(coalesce (altes.create_date, baixes.effective_date) AS date) AS data,
SUM(coalesce(altes.altes,0) -coalesce(baixes.baixes,0)) AS netes
FROM
	(SELECT s.create_date AS create_date, count(*) AS altes
	FROM odoo_subscription_request s
		LEFT JOIN odoo_res_partner p ON s.partner_id = p.id
	WHERE (s.type = 'new' OR s.type = 'subscription')
	  AND (s.state = 'done' OR s.state = 'draft' OR s.state = 'paid') and s.create_date>='20210101'
	GROUP BY s.create_date
) AS altes
FULL JOIN
(
	SELECT o.effective_date AS effective_date, count(*) AS baixes
	FROM odoo_operation_request o
	WHERE o.operation_type = 'sell_back' and effective_date>='20210101'
	GROUP BY o.effective_date
) AS baixes
ON altes.create_date = baixes.effective_date
GROUP BY CAST(coalesce (altes.create_date, baixes.effective_date) AS date);

DROP VIEW IF EXISTS odoo_projeccio_altes_socia CASCADE;
CREATE VIEW odoo_projeccio_altes_socia AS
SELECT data, SUM(i.amount)/dies_mes AS projeccio_altes_socia
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 677
and data<'20220101'
GROUP BY data, dies_mes
union all
SELECT data.data, SUM(p.persones_socies_netes)/cast(dies_mes as float) AS projeccio_altes_socia
FROM projeccions p
join data on  data.data>=p.data and data.data<p.data + INTERVAL '1 month'
WHERE data.data>='20220101'
GROUP BY data.data, dies_mes ;

DROP VIEW IF EXISTS odoo_projeccio_altes_usuaria CASCADE;
CREATE VIEW odoo_projeccio_altes_usuaria AS
SELECT data.data, SUM(p.usuaries_netes)/cast(dies_mes as float) AS projeccio_altes_usuaria
FROM projeccions p
join data on  data.data>=p.data and data.data<p.data + INTERVAL '1 month'
WHERE data.data>='20220101'
GROUP BY data.data, dies_mes ;


DROP VIEW IF EXISTS odoo_activacions_mobil CASCADE;
CREATE VIEW odoo_activacions_mobil AS
SELECT data, SUM(i.amount)/dies_mes AS activacions_mobil
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 672
 and date_FROM <='20201231'
GROUP BY data, dies_mes
UNION ALL
SELECT CAST(date_start AS date) AS date_start, count(*) AS count
FROM odoo_contract_contract
WHERE service_technology_id = 1
  AND note IS NULL
	and date_start>='20210101'
GROUP BY CAST(date_start AS date);


DROP VIEW IF EXISTS odoo_baixes_mobil CASCADE;
CREATE VIEW odoo_baixes_mobil AS
SELECT data, SUM(i.amount)/dies_mes AS baixes_mobil
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 337
 and date_FROM <='20201231'
GROUP BY data, dies_mes
UNION ALL
SELECT CAST(date_end AS date) AS date_end, count(*) AS count
FROM odoo_contract_contract
WHERE (service_technology_id = 1
  AND is_terminated = TRUE AND terminate_reASon_id NOT IN (5, 3, 9, 14, 4, 15, 8, 12))
  and date_end>='20210101'
GROUP BY CAST(date_end AS date);


DROP VIEW IF EXISTS odoo_projeccio_altes_mobil CASCADE;
CREATE VIEW odoo_projeccio_altes_mobil AS
SELECT data, SUM(i.amount)/dies_mes AS projeccio_altes_mobil
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 672
and data<'20220101'
GROUP BY data, dies_mes
union all
SELECT data.data, SUM(p.altes_netes_mobil)/cast(dies_mes as float) AS altes_netes_mobil
FROM projeccions p
join data on  data.data>=p.data and data.data<p.data + INTERVAL '1 month'
WHERE data.data>='20220101'
GROUP BY data.data, dies_mes ;
;

DROP VIEW IF EXISTS odoo_activacions_fibra CASCADE;
CREATE VIEW odoo_activacions_fibra AS
SELECT data, SUM(i.amount)/dies_mes AS activacions_fibra
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 671
 and date_FROM <='20201231'
GROUP BY data, dies_mes
UNION ALL
SELECT CAST(date_start AS date) AS date_start, count(*) AS count
FROM odoo_contract_contract
WHERE service_technology_id = 3
  AND note is null
  and date_start>='20210101'
GROUP BY CAST(date_start AS date);


DROP VIEW IF EXISTS odoo_baixes_fibra CASCADE;
CREATE VIEW odoo_baixes_fibra AS
SELECT data, SUM(i.amount)/dies_mes AS baixes_fibra
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 392
 and date_FROM <='20201231'
GROUP BY data, dies_mes
UNION ALL
SELECT CAST(date_end AS date) AS date_end, count(*) AS count
FROM odoo_contract_contract
WHERE (service_technology_id = 3
    AND is_terminated = TRUE AND terminate_reASon_id NOT IN (5, 3, 9, 14, 4, 15, 8, 12))
	and date_end>='20210101'
GROUP BY CAST(date_end AS date) ;


DROP VIEW IF EXISTS odoo_projeccio_altes_fibra CASCADE;
create VIEW odoo_projeccio_altes_fibra AS
SELECT data, SUM(i.amount)/dies_mes AS projeccio_altes_fibra
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 671
and data<'20220101'
GROUP BY data, dies_mes
union all
SELECT data.data, SUM(p.altes_netes_BA)/cast(dies_mes as float) AS altes_netes_BA
FROM projeccions p
join data on  data.data>=p.data and data.data<p.data + INTERVAL '1 month'
WHERE data.data>='20220101'
GROUP BY data.data, dies_mes ;

DROP VIEW IF EXISTS odoo_activacions_adsl CASCADE;
CREATE VIEW odoo_activacions_adsl AS
SELECT CAST(date_start AS date) AS data, count(*) AS count
FROM odoo_contract_contract
WHERE service_technology_id = 2
  AND note IS NULL
	and date_start>='20210101'
GROUP BY CAST(date_start AS date);

DROP VIEW IF EXISTS odoo_baixes_adsl CASCADE;
CREATE VIEW odoo_baixes_adsl AS
SELECT CAST(date_end AS date) AS data, count(*) AS count
FROM odoo_contract_contract
WHERE (service_technology_id = 2
  AND is_terminated = TRUE AND terminate_reASon_id NOT IN (5, 3, 9, 14, 4, 15, 8, 12))
  and date_end>='20210101'
GROUP BY CAST(date_end AS date);

DROP VIEW IF EXISTS odoo_activacions_4g CASCADE;
CREATE VIEW odoo_activacions_4g AS
SELECT CAST(date_start AS date) AS data, count(*) AS count
FROM odoo_contract_contract
WHERE service_technology_id = 4
  AND note IS NULL
	and date_start>='20210101'
GROUP BY CAST(date_start AS date);

DROP VIEW IF EXISTS odoo_baixes_4g CASCADE;
CREATE VIEW odoo_baixes_4g AS
SELECT CAST(date_end AS date) AS data, count(*) AS count
FROM odoo_contract_contract
WHERE (service_technology_id = 4
  AND is_terminated = TRUE AND terminate_reASon_id NOT IN (5, 3, 9, 14, 4, 15, 8, 12))
  and date_end>='20210101'
GROUP BY CAST(date_end AS date);

DROP VIEW IF EXISTS odoo_pet_contracte CASCADE;
CREATE VIEW odoo_pet_contracte as
SELECT data, SUM(i.amount)/dies_mes AS pet_contracte
FROM odoo_mis_budget_item i
LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
WHERE k.kpi_id = 670
 and date_FROM <='20201231'
GROUP BY data, dies_mes
UNION ALL
   SELECT cast(l.create_date as date) AS create_date,
   count(*) AS count
FROM odoo_crm_lead_line l
LEFT JOIN odoo_crm_lead c ON l.lead_id = c.id
WHERE (
AND c.create_date >='2021-01-01'
    AND l.create_date >= timestamp with time zone '2021-01-11 00:00:00.000Z'
)
GROUP BY cast(l.create_date as date);


drop view if exists odoo_baixes_apadrinades CASCADE;
create view odoo_baixes_apadrinades as
select date_end as data, count(*) as baixes_apadrinades
from (
select   cc.partner_id, cc.date_start, cc.date_end
from odoo_contract_contract cc
	join odoo_res_partner rp on cc.partner_id =rp.id
where coop_sponsee = true
	and date_end is not null
	and not exists (
	select *
	from odoo_contract_contract c1
	where c1.partner_id =cc.partner_id
		and c1.date_start >= cc.date_end
	)
) a
group by  date_end;

drop view if exists odoo_baixes_intercooperacio CASCADE;
create view odoo_baixes_intercooperacio as
select date_end as data, count(*) as baixes_intercooperacio
from (
select   cc.partner_id, cc.date_start, cc.date_end
from odoo_contract_contract cc
	join odoo_res_partner rp on cc.partner_id =rp.id
where coop_agreement = true
	and date_end is not null
	and not exists (
	select *
	from odoo_contract_contract c1
	where c1.partner_id =cc.partner_id
		and c1.date_start >= cc.date_end
	)
) a
group by  date_end;


drop view if exists odoo_altes_apadrinades CASCADE;
create view odoo_altes_apadrinades as
select date_start as data, count(*) as altes_apadrinades
from
(
select distinct cc.partner_id,  cc.date_start, cc.date_end
from odoo_contract_contract cc
	join odoo_res_partner rp on cc.partner_id =rp.id
where coop_sponsee = true
	and not exists (
	select *
	from odoo_contract_contract c1
	where c1.partner_id =cc.partner_id
		and c1.date_start > cc.date_start
	)
) a
group by date_start;


drop view if exists odoo_altes_intercooperacio CASCADE;
create view odoo_altes_intercooperacio as
select date_start as data, count(*) as altes_intercooperacio
from
(
select distinct cc.partner_id,  cc.date_start, cc.date_end
from odoo_contract_contract cc
	join odoo_res_partner rp on cc.partner_id =rp.id
where coop_agreement = true
	and not exists (
	select *
	from odoo_contract_contract c1
	where c1.partner_id =cc.partner_id
		and c1.date_start > cc.date_start
	)
) a
group by date_start;

drop view if exists odoo_noves_usuaries CASCADE;
create view odoo_noves_usuaries as
select date::timestamp without time zone,
case type
	when 'new' then 'Nova sòcia'
	when 'sponsorship' then 'Apadrinament'
	when 'sponsorship_coop_agreement' then 'Apadrinament amb conveni cooperatiu'
end as type
, count(*) as noves_usuaries
from odoo_subscription_request
where state not in ('cancelled')
group by date, case type
	when 'new' then 'Nova sòcia'
	when 'sponsorship' then 'Apadrinament'
	when 'sponsorship_coop_agreement' then 'Apadrinament amb conveni cooperatiu'
end;





DROP VIEW IF EXISTS odoo_indicadors_socies_mobil_fibra CASCADE;
CREATE VIEW odoo_indicadors_socies_mobil_fibra AS
SELECT data, date_part('YEAR', data) as any, date_part('MONTH', data) as mes, date_part('YEAR', data) *100+ date_part('MONTH', data) as any_mes,
date_part('WEEK', data) as setmana, date_part('DOW', data) as dia_de_la_setmana
,SUM(altes_socia) AS altes_socia, SUM(baixes_socia) AS baixes_socia, SUM(altes_netes_socia) AS altes_netes_socia, SUM(projeccio_altes_socia) AS projeccio_altes_socia
	, SUM(activacions_mobil) AS activacions_mobil, SUM(baixes_mobil) AS baixes_mobil, SUM(projeccio_altes_mobil) AS projeccio_altes_mobil
	, SUM(activacions_fibra) AS activacions_fibra, SUM(baixes_fibra) AS baixes_fibra, SUM(projeccio_altes_fibra) AS projeccio_altes_fibra
	, SUM(pet_contracte) as pet_contracte
	, sum(baixes_apadrinades) as baixes_apadrinades, sum(baixes_intercooperacio) as baixes_intercooperacio
	, sum(altes_apadrinades) as altes_apadrinades, sum(altes_intercooperacio) as altes_intercooperacio
	, sum(projeccio_altes_usuaria) as projeccio_altes_usuaria
	, SUM(activacions_adsl) AS activacions_adsl, SUM(baixes_adsl) AS baixes_adsl
	, SUM(activacions_4g) AS activacions_4g, SUM(baixes_4g) AS baixes_4g
FROM (
	SELECT data
	, altes_socia, 0 AS baixes_socia, 0 AS altes_netes_socia, 0 AS projeccio_altes_socia
	, 0 AS activacions_mobil, 0 AS baixes_mobil, 0 AS projeccio_altes_mobil
	, 0 AS activacions_fibra, 0 AS baixes_fibra, 0 AS projeccio_altes_fibra
	, 0 AS pet_contracte
	,0 as baixes_apadrinades, 0 as baixes_intercooperacio, 0 as altes_apadrinades, 0 as altes_intercooperacio,0 as projeccio_altes_usuaria
	,0 as activacions_adsl,0 as baixes_adsl,0 as activacions_4g,0 as baixes_4g
	FROM odoo_altes_socia x
	UNION ALL
	SELECT data, 0, x.baixes_socia,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	FROM odoo_baixes_socia x
	UNION ALL
	SELECT data, 0, 0, altes_netes_socia,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	FROM odoo_altes_netes_socia
	UNION ALL
	SELECT data, 0,0,0, x.projeccio_altes_socia,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	FROM odoo_projeccio_altes_socia x
	UNION ALL
	SELECT data,0,0,0,0, activacions_mobil,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	FROM odoo_activacions_mobil
	UNION ALL
	SELECT data ,0,0,0,0,0, baixes_mobil,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	FROM odoo_baixes_mobil
	UNION ALL
	SELECT data, 0,0,0,0,0,0,projeccio_altes_mobil,0,0,0,0,0,0,0,0,0,0,0,0,0
	FROM odoo_projeccio_altes_mobil
	UNION ALL
	SELECT data, 0,0,0,0,0,0,0, activacions_fibra,0,0,0,0,0,0,0,0,0,0,0,0
	FROM odoo_activacions_fibra
	UNION ALL
	SELECT data, 0,0,0,0,0,0,0,0, baixes_fibra,0,0,0,0,0,0,0,0,0,0,0
	FROM odoo_baixes_fibra
	UNION ALL
	SELECT data, 0,0,0,0,0,0,0,0,0, projeccio_altes_fibra,0,0,0,0,0,0,0,0,0,0
	FROM odoo_projeccio_altes_fibra
	UNION ALL
	SELECT data, 0,0,0,0,0,0,0,0,0,0,pet_contracte,0,0,0,0,0,0,0,0,0
	FROM odoo_pet_contracte
	union all
	select data, 0,0,0,0,0,0,0,0,0,0,0,baixes_apadrinades,0,0,0,0,0,0,0,0
	from odoo_baixes_apadrinades
	union all
	select data, 0,0,0,0,0,0,0,0,0,0,0,0,baixes_intercooperacio,0,0,0,0,0,0,0
	from odoo_baixes_intercooperacio
	union all
	select data, 0,0,0,0,0,0,0,0,0,0,0,0,0,altes_apadrinades,0,0,0,0,0,0
	from odoo_altes_apadrinades
	union all
	select data, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,altes_intercooperacio,0,0,0,0,0
	from odoo_altes_intercooperacio
	union all
	select data, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,projeccio_altes_usuaria,0,0,0,0
	from odoo_projeccio_altes_usuaria
	union all
	select data, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, count, 0,0,0
	from odoo_activacions_adsl
	union all
	select data, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,count,0,0
	from odoo_baixes_adsl
	union all
	select data, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,count,0
	from odoo_activacions_4g
	union all
	select data, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,count
	from odoo_baixes_4g
) s
where data>='20190101'
GROUP BY data;


DROP VIEW IF EXISTS odoo_indicadors_mes_socies_mobil_fibra CASCADE;
CREATE VIEW odoo_indicadors_mes_socies_mobil_fibra AS
SELECT cast(date_part('YEAR', data) as smallint) as any, cast(date_part('MONTH', data) as smallint) as mes, date_part('YEAR', data) *100+ date_part('MONTH', data) as any_mes
,SUM(altes_socia) AS altes_socia, SUM(baixes_socia) AS baixes_socia, SUM(altes_netes_socia) AS altes_netes_socia, SUM(projeccio_altes_socia) AS projeccio_altes_socia
	, SUM(activacions_mobil) AS activacions_mobil, SUM(baixes_mobil) AS baixes_mobil, SUM(projeccio_altes_mobil) AS projeccio_altes_mobil
	, SUM(activacions_fibra) AS activacions_fibra, SUM(baixes_fibra) AS baixes_fibra, SUM(projeccio_altes_fibra) AS projeccio_altes_fibra
	, SUM(pet_contracte) as pet_contracte
	,sum(baixes_apadrinades) as baixes_apadrinades, sum(baixes_intercooperacio) as baixes_intercooperacio
	,sum(altes_apadrinades) as altes_apadrinades, sum(altes_intercooperacio) as altes_intercooperacio
	,	date_trunc('year', data)::timestamp without time zone as data_inici_any
	, min(data)::timestamp without time zone as data_inici_mes, data::timestamp without time zone as data
	, sum(projeccio_altes_usuaria) as projeccio_altes_usuaria
FROM odoo_indicadors_socies_mobil_fibra
GROUP BY date_part('YEAR', data), date_part('MONTH', data),	date_trunc('year', data)::timestamp without time zone, data::timestamp without time zone ;


DROP VIEW IF EXISTS odoo_indicadors_dia_socies_mobil_fibra_no_projeccio CASCADE;
CREATE VIEW odoo_indicadors_dia_socies_mobil_fibra_no_projeccio AS
SELECT data::timestamp without time zone as data,cast(date_part('YEAR', data) as smallint) as any, cast(date_part('MONTH', data) as smallint) as mes, date_part('YEAR', data) *100+ date_part('MONTH', data) as any_mes,
cast(date_part('WEEK', data) as smallint) as setmana, date_part('DOW', data) as dia_de_la_setmana
,SUM(altes_socia) AS altes_socia, SUM(baixes_socia) AS baixes_socia, SUM(altes_netes_socia) AS altes_netes_socia
	, SUM(activacions_mobil) AS activacions_mobil, SUM(baixes_mobil) AS baixes_mobil
	, SUM(activacions_fibra) AS activacions_fibra, SUM(baixes_fibra) AS baixes_fibra
	, SUM(pet_contracte) as pet_contracte
	,sum(baixes_apadrinades) as baixes_apadrinades, sum(baixes_intercooperacio) as baixes_intercooperacio
	,sum(altes_apadrinades) as altes_apadrinades, sum(altes_intercooperacio) as altes_intercooperacio
	,(cast('1900-01-01' as date) + ((date_part('Month',date_trunc('month', data))-1) || ' months')::INTERVAL)::timestamp without time zone as data_normalitzada_mes
 ,(cast('1900-01-01' as date) + ((date_part('week',date_trunc('week', data))-1) || ' week')::INTERVAL)::timestamp without time zone  as data_normalitzada_setmana
 , sum(projeccio_altes_usuaria) as projeccio_altes_usuaria
FROM odoo_indicadors_socies_mobil_fibra
GROUP BY data::timestamp without time zone, data;

DROP VIEW IF EXISTS odoo_indicadors_dia_socies_mobil_fibra_amb_projeccio CASCADE;
CREATE VIEW odoo_indicadors_dia_socies_mobil_fibra_amb_projeccio AS
SELECT data::timestamp without time zone as data,cast(date_part('YEAR', data) as smallint) as any, cast(date_part('MONTH', data) as smallint) as mes, date_part('YEAR', data) *100+ date_part('MONTH', data) as any_mes,
cast(date_part('WEEK', data) as smallint) as setmana, date_part('DOW', data) as dia_de_la_setmana
,SUM(altes_socia) AS altes_socia, SUM(baixes_socia) AS baixes_socia, SUM(altes_netes_socia) AS altes_netes_socia
	, SUM(activacions_mobil) AS activacions_mobil, SUM(baixes_mobil) AS baixes_mobil
	, SUM(activacions_fibra) AS activacions_fibra, SUM(baixes_fibra) AS baixes_fibra
	, SUM(pet_contracte) as pet_contracte
	,sum(baixes_apadrinades) as baixes_apadrinades, sum(baixes_intercooperacio) as baixes_intercooperacio
	,sum(altes_apadrinades) as altes_apadrinades, sum(altes_intercooperacio) as altes_intercooperacio
	,(cast('1900-01-01' as date) + ((date_part('Month',date_trunc('month', data))-1) || ' months')::INTERVAL)::timestamp without time zone as data_normalitzada_mes
 ,(cast('1900-01-01' as date) + ((date_part('week',date_trunc('week', data))-1) || ' week')::INTERVAL)::timestamp without time zone  as data_normalitzada_setmana
 , SUM(projeccio_altes_socia) AS projeccio_altes_socia, SUM(projeccio_altes_mobil) as projeccio_altes_mobil, SUM(projeccio_altes_fibra) as projeccio_altes_fibra
 , sum(projeccio_altes_usuaria) as projeccio_altes_usuaria
FROM odoo_indicadors_socies_mobil_fibra
GROUP BY data::timestamp without time zone, data;


DROP VIEW IF EXISTS odoo_indicadors_mes_socies_mobil_fibra_mes_tancat CASCADE;
CREATE VIEW odoo_indicadors_mes_socies_mobil_fibra_mes_tancat AS
SELECT  cast(date_part('YEAR', data) as smallint) as any, cast(date_part('MONTH', data) as smallint) as mes, date_part('YEAR', data) *100+ date_part('MONTH', data) as any_mes
,SUM(altes_socia) AS altes_socia, SUM(baixes_socia) AS baixes_socia, SUM(altes_netes_socia) AS altes_netes_socia, SUM(projeccio_altes_socia) AS projeccio_altes_socia
	, SUM(activacions_mobil) AS activacions_mobil, SUM(baixes_mobil) AS baixes_mobil, SUM(projeccio_altes_mobil) AS projeccio_altes_mobil
	, SUM(activacions_fibra) AS activacions_fibra, SUM(baixes_fibra) AS baixes_fibra, SUM(projeccio_altes_fibra) AS projeccio_altes_fibra
	, SUM(pet_contracte) as pet_contracte
	,sum(baixes_apadrinades) as baixes_apadrinades, sum(baixes_intercooperacio) as baixes_intercooperacio
	,sum(altes_apadrinades) as altes_apadrinades, sum(altes_intercooperacio) as altes_intercooperacio
	,date_trunc('year', data)::DATE as data
	, sum(projeccio_altes_usuaria) as projeccio_altes_usuaria
FROM odoo_indicadors_socies_mobil_fibra
where date_part('Month', data)<=date_part('Month',date_trunc('month', current_date-interval '1 month'))
GROUP BY date_part('YEAR', data), date_part('MONTH', data), date_trunc('year', data) ;

DROP VIEW IF EXISTS odoo_indicadors_dia_socies_mobil_fibra_no_projeccio_mes_tancat CASCADE;
CREATE VIEW odoo_indicadors_dia_socies_mobil_fibra_no_projeccio_mes_tancat AS
SELECT
(cast('1900-01-01' as date) + ((date_part('Month',date_trunc('month', data))-1) || ' months')::INTERVAL)::DATE as data_normalitzada_mes,
(cast('1900-01-01' as date) + ((date_part('week',date_trunc('week', data))-1) || ' week')::INTERVAL)::DATE  as data_normalitzada_setmana,
data, cast(date_part('YEAR', data) as smallint) as any, cast(date_part('MONTH', data) as smallint) as mes, date_part('YEAR', data) *100+ date_part('MONTH', data) as any_mes,
date_part('WEEK', data) as setmana, date_part('DOW', data) as dia_de_la_setmana
,SUM(altes_socia) AS altes_socia, SUM(baixes_socia) AS baixes_socia, SUM(altes_netes_socia) AS altes_netes_socia
	, SUM(activacions_mobil) AS activacions_mobil, SUM(baixes_mobil) AS baixes_mobil
	, SUM(activacions_fibra) AS activacions_fibra, SUM(baixes_fibra) AS baixes_fibra
	, SUM(pet_contracte) as pet_contracte
	, sum(baixes_apadrinades) as baixes_apadrinades, sum(baixes_intercooperacio) as baixes_intercooperacio
	, sum(altes_apadrinades) as altes_apadrinades, sum(altes_intercooperacio) as altes_intercooperacio
	, sum(projeccio_altes_usuaria) as projeccio_altes_usuaria
FROM odoo_indicadors_socies_mobil_fibra
where date_part('Month', data)<=date_part('Month',date_trunc('month', current_date-interval '1 month'))
GROUP BY data;

drop view v_peticions_contracte_tecnologia cascade;
create or replace view v_peticions_contracte_tecnologia as
  SELECT cast(l.create_date as date) AS create_date,
  case pc.complete_name
  when 'Broadband Service / Broadband ADSL Service' then 'ADSL'
  when 'Broadband Service / Broadband Fiber Service' then 'Fibra'
  when 'Mobile Service' then 'Mòbil'
  else 'Altres' end as tecnologia,
   count(*) AS peticions_contracte
FROM odoo_crm_lead_line l
	join odoo_product_category pc on l.category_id =pc.id
	LEFT JOIN odoo_crm_lead c ON l.lead_id = c.id
WHERE (
	c.create_date >='2021-01-01'
    AND l.create_date >= timestamp with time zone '2021-01-11 00:00:00.000Z'
)
GROUP BY cast(l.create_date as date),
case pc.complete_name
  when 'Broadband Service / Broadband ADSL Service' then 'ADSL'
  when 'Broadband Service / Broadband Fiber Service' then 'Fibra'
  when 'Mobile Service' then 'Mòbil'
  else 'Altres' end;


DROP VIEW IF EXISTS v_altes_baixes_projeccions_tecnologia_proveidor CASCADE;
create view v_altes_baixes_projeccions_tecnologia_proveidor as
select date_start, technology, provider, sum(altes) as altes, sum(baixes) as baixes, sum(altes) - sum(baixes) as altes_netes, sum(projeccio) as projeccio
from  (
        SELECT CAST(date_start AS date) AS date_start, ost."name"  as technology, oss."name"  as provider, count(*) AS altes,0 as baixes, 0 as projeccio
        FROM odoo_contract_contract occ
            join odoo_service_technology ost on occ.service_technology_id =ost.id
            join odoo_service_supplier oss on occ.service_supplier_id = oss.id
        WHERE  note is null
          and date_start>='20210101'
        GROUP BY CAST(date_start AS date),  ost."name", oss."name"
    union all
        SELECT CAST(date_end AS date) AS date_end, ost."name"  as technology, oss."name"  as provider, 0, count(*) AS baixes, 0
        FROM odoo_contract_contract occ
            join odoo_service_technology ost on occ.service_technology_id =ost.id
            join odoo_service_supplier oss on occ.service_supplier_id = oss.id
        WHERE (is_terminated = TRUE AND terminate_reASon_id NOT IN (5, 3, 9, 14, 4, 15, 8, 12))
            and date_end>='20210101'
        GROUP BY CAST(date_end AS date),  ost."name", oss."name"
    union all
        SELECT data.data,'Fiber', 'Projecció', 0, 0, SUM(p.altes_netes_BA)/cast(dies_mes as float) AS projeccio
        FROM projeccions p
            join data on  data.data>=p.data and data.data<p.data + INTERVAL '1 month'
        WHERE data.data>='20220101'
        GROUP BY data.data, dies_mes
    union all
        SELECT data.data,'Mobile', 'Projecció', 0, 0, SUM(p.altes_netes_mobil)/cast(dies_mes as float) AS projeccio
        FROM projeccions p
            join data on  data.data>=p.data and data.data<p.data + INTERVAL '1 month'
        WHERE data.data>='20220101'
        GROUP BY data.data, dies_mes
    union all
    	SELECT data, 'Mobile', 'Projecció', 0, 0, SUM(i.amount)/dies_mes AS projeccio_altes_mobil
	FROM odoo_mis_budget_item i
		LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
		join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
	WHERE k.kpi_id = 672
		and data<'20220101'
	group by data, dies_mes
    union all
	SELECT data,'Fiber', 'Projecció', 0, 0, SUM(i.amount)/dies_mes AS projeccio_altes_fibra
	FROM odoo_mis_budget_item i
		LEFT JOIN odoo_mis_report_kpi_expression k ON i.kpi_expression_id = k.id
		join data on  data.data>=date_from and data.data<date_from + INTERVAL '1 month'
	WHERE k.kpi_id = 671
		and data<'20220101'
	GROUP BY data, dies_mes
)a
group by date_start, technology, provider;