create view v_odoo_activity as
select  ma.date_deadline, cast(ma.create_date as date) as create_date, ma.date_done, mat.name as activity_type_name
,im.name as origen
,ma.date_done-ma.date_deadline as days_until_deadline --negatiu = s'ha fet abans del dia límit
,ma.date_done-cast(ma.create_date as date) as days_from_create -- dies que s'ha trigat a fer
from odoo_mail_activity ma
	join odoo_mail_activity_type mat on ma.activity_type_id =mat.id
	join odoo_ir_model im on ma.res_model_id = im.id;
