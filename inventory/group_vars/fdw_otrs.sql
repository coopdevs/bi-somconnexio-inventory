 drop foreign table if exists otrs_access_token;
create FOREIGN TABLE otrs_access_token("id" bigint NOT NULL
,"uuid" varchar(100) NOT NULL
,"token" text NOT NULL
,"user_id" varchar(255) NOT NULL
,"user_type" varchar(20) NOT NULL
,"create_time" timestamp NOT NULL
,"expires_time" timestamp NOT NULL
,"last_access_time" timestamp NOT NULL
,"is_counted_for_limits" smallint NOT NULL
,"is_interactive" smallint NOT NULL
,"from_client" varchar(50))SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'access_token');

 drop foreign table if exists otrs_access_token_key;
create FOREIGN TABLE otrs_access_token_key("id" bigint NOT NULL
,"val" varchar(100) NOT NULL
,"create_time" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'access_token_key');

 drop foreign table if exists otrs_acl;
create FOREIGN TABLE otrs_acl("id" int NOT NULL
,"name" varchar(200) NOT NULL
,"comments" varchar(250)
,"description" varchar(250)
,"valid_id" smallint NOT NULL
,"stop_after_match" smallint
,"config_match" text
,"config_change" text
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'acl');

 drop foreign table if exists otrs_acl_deployment;
create FOREIGN TABLE otrs_acl_deployment("id" int NOT NULL
,"uuid" varchar(36) NOT NULL
,"effective_value" text NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'acl_deployment');

 drop foreign table if exists otrs_acl_sync;
create FOREIGN TABLE otrs_acl_sync("acl_id" varchar(200) NOT NULL
,"sync_state" varchar(30) NOT NULL
,"create_time" timestamp NOT NULL
,"change_time" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'acl_sync');

 drop foreign table if exists otrs_article;
create FOREIGN TABLE otrs_article("id" bigint NOT NULL
,"ticket_id" bigint NOT NULL
,"article_sender_type_id" smallint NOT NULL
,"communication_channel_id" bigint NOT NULL
,"is_visible_for_customer" smallint NOT NULL
,"search_index_needs_rebuild" smallint NOT NULL
,"insert_fingerprint" varchar(64)
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'article');

 drop foreign table if exists otrs_article_customer_flag;
create FOREIGN TABLE otrs_article_customer_flag("article_id" bigint NOT NULL
,"article_key" varchar(50) NOT NULL
,"article_value" varchar(50)
,"create_time" timestamp NOT NULL
,"login" varchar(200) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'article_customer_flag');

 drop foreign table if exists otrs_article_data_mime;
create FOREIGN TABLE otrs_article_data_mime("id" bigint NOT NULL
,"article_id" bigint NOT NULL
,"a_from" varchar(500)
,"a_reply_to" varchar(500)
,"a_to" varchar(500)
,"a_cc" varchar(500)
,"a_bcc" varchar(500)
,"a_subject" text
,"a_message_id" text
,"a_message_id_md5" varchar(32)
,"a_in_reply_to" varchar(500)
,"a_references" varchar(500)
,"a_content_type" varchar(250)
,"a_body" varchar(500)
,"incoming_time" int NOT NULL
,"content_path" varchar(250)
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'article_data_mime');

 drop foreign table if exists otrs_article_data_mime_attachment;
create FOREIGN TABLE otrs_article_data_mime_attachment("id" bigint NOT NULL
,"article_id" bigint NOT NULL
,"filename" varchar(250)
,"content_size" varchar(30)
,"content_type" text
,"content_id" varchar(250)
,"content_alternative" varchar(50)
,"disposition" varchar(15)
,"content" text
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'article_data_mime_attachment');

 drop foreign table if exists otrs_article_data_mime_plain;
create FOREIGN TABLE otrs_article_data_mime_plain("id" bigint NOT NULL
,"article_id" bigint NOT NULL
,"body" text NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'article_data_mime_plain');

 drop foreign table if exists otrs_article_data_mime_send_error;
create FOREIGN TABLE otrs_article_data_mime_send_error("id" bigint NOT NULL
,"article_id" bigint NOT NULL
,"message_id" varchar(200)
,"log_message" varchar(500)
,"create_time" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'article_data_mime_send_error');

 drop foreign table if exists otrs_article_data_otrs_chat;
create FOREIGN TABLE otrs_article_data_otrs_chat("id" bigint NOT NULL
,"article_id" bigint NOT NULL
,"chat_participant_id" varchar(255) NOT NULL
,"chat_participant_name" varchar(255) NOT NULL
,"chat_participant_type" varchar(255) NOT NULL
,"message_text" text
,"system_generated" smallint NOT NULL
,"create_time" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'article_data_otrs_chat');

 drop foreign table if exists otrs_article_data_otrs_sms;
create FOREIGN TABLE otrs_article_data_otrs_sms("id" bigint NOT NULL
,"article_id" bigint NOT NULL
,"sender_string" varchar(255) NOT NULL
,"phone_numbers" varchar(500) NOT NULL
,"text" varchar(500) NOT NULL
,"is_flash_message" smallint NOT NULL
,"transaction_numbers" varchar(500) NOT NULL
,"create_time" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'article_data_otrs_sms');

 drop foreign table if exists otrs_article_flag;
create FOREIGN TABLE otrs_article_flag("article_id" bigint NOT NULL
,"article_key" varchar(50) NOT NULL
,"article_value" varchar(50)
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'article_flag');

 drop foreign table if exists otrs_article_search_index;
create FOREIGN TABLE otrs_article_search_index("id" bigint NOT NULL
,"ticket_id" bigint NOT NULL
,"article_id" bigint NOT NULL
,"article_key" varchar(200) NOT NULL
,"article_value" varchar(500))SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'article_search_index');

 drop foreign table if exists otrs_article_sender_type;
create FOREIGN TABLE otrs_article_sender_type("id" smallint NOT NULL
,"name" varchar(200) NOT NULL
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'article_sender_type');

 drop foreign table if exists otrs_auto_response;
create FOREIGN TABLE otrs_auto_response("id" int NOT NULL
,"name" varchar(200) NOT NULL
,"text0" text
,"text1" text
,"type_id" smallint NOT NULL
,"system_address_id" smallint NOT NULL
,"content_type" varchar(250)
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'auto_response');

 drop foreign table if exists otrs_auto_response_type;
create FOREIGN TABLE otrs_auto_response_type("id" smallint NOT NULL
,"name" varchar(200) NOT NULL
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'auto_response_type');

 drop foreign table if exists otrs_calendar;
create FOREIGN TABLE otrs_calendar("id" bigint NOT NULL
,"group_id" int NOT NULL
,"name" varchar(200) NOT NULL
,"salt_string" varchar(64) NOT NULL
,"color" varchar(7) NOT NULL
,"ticket_appointments" text
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'calendar');

 drop foreign table if exists otrs_calendar_appointment;
create FOREIGN TABLE otrs_calendar_appointment("id" bigint NOT NULL
,"parent_id" bigint
,"calendar_id" bigint NOT NULL
,"unique_id" varchar(255) NOT NULL
,"title" varchar(255) NOT NULL
,"description" text
,"location" varchar(255)
,"start_time" timestamp NOT NULL
,"end_time" timestamp NOT NULL
,"all_day" smallint
,"notify_time" timestamp
,"notify_template" varchar(255)
,"notify_custom" varchar(255)
,"notify_custom_unit_count" bigint
,"notify_custom_unit" varchar(255)
,"notify_custom_unit_point" varchar(255)
,"notify_custom_date" timestamp
,"team_id" text
,"resource_id" text
,"recurring" smallint
,"recur_type" varchar(20)
,"recur_freq" varchar(255)
,"recur_count" int
,"recur_interval" int
,"recur_until" timestamp
,"recur_id" timestamp
,"recur_exclude" text
,"ticket_appointment_rule_id" varchar(32)
,"create_time" timestamp
,"create_by" int
,"change_time" timestamp
,"change_by" int)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'calendar_appointment');

 drop foreign table if exists otrs_calendar_appointment_ticket;
create FOREIGN TABLE otrs_calendar_appointment_ticket("calendar_id" bigint NOT NULL
,"ticket_id" bigint NOT NULL
,"rule_id" varchar(32) NOT NULL
,"appointment_id" bigint NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'calendar_appointment_ticket');

 drop foreign table if exists otrs_calendar_setting;
create FOREIGN TABLE otrs_calendar_setting("id" bigint NOT NULL
,"calendar_id" bigint NOT NULL
,"config" text NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'calendar_setting');

 drop foreign table if exists otrs_calendar_team;
create FOREIGN TABLE otrs_calendar_team("id" bigint NOT NULL
,"name" varchar(200) NOT NULL
,"group_id" int NOT NULL
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'calendar_team');

 drop foreign table if exists otrs_calendar_team_user;
create FOREIGN TABLE otrs_calendar_team_user("user_id" int NOT NULL
,"team_id" int NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'calendar_team_user');

 drop foreign table if exists otrs_change_cab;
create FOREIGN TABLE otrs_change_cab("id" bigint NOT NULL
,"change_id" bigint NOT NULL
,"user_id" int
,"customer_user_id" varchar(250))SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'change_cab');

 drop foreign table if exists otrs_change_cip_allocate;
create FOREIGN TABLE otrs_change_cip_allocate("id" bigint NOT NULL
,"category_id" int NOT NULL
,"impact_id" int NOT NULL
,"priority_id" int NOT NULL
,"create_time" timestamp
,"create_by" int
,"change_time" timestamp
,"change_by" int)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'change_cip_allocate');

 drop foreign table if exists otrs_change_condition;
create FOREIGN TABLE otrs_change_condition("id" bigint NOT NULL
,"change_id" bigint NOT NULL
,"name" varchar(250) NOT NULL
,"expression_conjunction" varchar(250) NOT NULL
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'change_condition');

 drop foreign table if exists otrs_change_history;
create FOREIGN TABLE otrs_change_history("id" bigint NOT NULL
,"change_id" bigint NOT NULL
,"workorder_id" bigint
,"type_id" int NOT NULL
,"content_new" varchar(500)
,"content_old" varchar(500)
,"fieldname" varchar(250)
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'change_history');

 drop foreign table if exists otrs_change_history_type;
create FOREIGN TABLE otrs_change_history_type("id" int NOT NULL
,"name" varchar(100) NOT NULL
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'change_history_type');

 drop foreign table if exists otrs_change_item;
create FOREIGN TABLE otrs_change_item("id" bigint NOT NULL
,"change_number" varchar(100) NOT NULL
,"title" varchar(250)
,"description" varchar(500)
,"description_plain" varchar(500)
,"justification" varchar(500)
,"justification_plain" varchar(500)
,"change_state_id" int NOT NULL
,"change_manager_id" int
,"change_builder_id" int NOT NULL
,"category_id" int NOT NULL
,"impact_id" int NOT NULL
,"priority_id" int NOT NULL
,"requested_time" timestamp
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'change_item');

 drop foreign table if exists otrs_change_notification;
create FOREIGN TABLE otrs_change_notification("id" bigint NOT NULL
,"name" varchar(250) NOT NULL
,"item_attribute" varchar(250)
,"event_id" int NOT NULL
,"valid_id" smallint NOT NULL
,"comments" varchar(250)
,"notification_rule" varchar(250))SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'change_notification');

 drop foreign table if exists otrs_change_notification_grps;
create FOREIGN TABLE otrs_change_notification_grps("id" bigint NOT NULL
,"name" varchar(250) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'change_notification_grps');

 drop foreign table if exists otrs_change_notification_message;
create FOREIGN TABLE otrs_change_notification_message("id" bigint NOT NULL
,"notification_id" bigint NOT NULL
,"subject" varchar(200) NOT NULL
,"text" text NOT NULL
,"content_type" varchar(250) NOT NULL
,"language" varchar(60) NOT NULL
,"notification_type" varchar(60) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'change_notification_message');

 drop foreign table if exists otrs_change_notification_rec;
create FOREIGN TABLE otrs_change_notification_rec("id" bigint NOT NULL
,"notification_id" bigint NOT NULL
,"group_id" bigint NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'change_notification_rec');

 drop foreign table if exists otrs_change_number_counter;
create FOREIGN TABLE otrs_change_number_counter("id" bigint NOT NULL
,"counter" bigint NOT NULL
,"counter_uid" varchar(32) NOT NULL
,"create_time" timestamp)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'change_number_counter');

 drop foreign table if exists otrs_change_state_machine;
create FOREIGN TABLE otrs_change_state_machine("id" bigint NOT NULL
,"state_id" int
,"next_state_id" int)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'change_state_machine');

 drop foreign table if exists otrs_change_template;
create FOREIGN TABLE otrs_change_template("id" int NOT NULL
,"name" varchar(250) NOT NULL
,"comments" varchar(250)
,"content" text NOT NULL
,"type_id" int NOT NULL
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'change_template');

 drop foreign table if exists otrs_change_template_type;
create FOREIGN TABLE otrs_change_template_type("id" int NOT NULL
,"name" varchar(250) NOT NULL
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'change_template_type');

 drop foreign table if exists otrs_change_workorder;
create FOREIGN TABLE otrs_change_workorder("id" bigint NOT NULL
,"change_id" bigint NOT NULL
,"workorder_number" int NOT NULL
,"title" varchar(250)
,"instruction" varchar(500)
,"instruction_plain" varchar(500)
,"report" varchar(500)
,"report_plain" varchar(500)
,"workorder_state_id" int NOT NULL
,"workorder_type_id" int NOT NULL
,"workorder_agent_id" int
,"planned_start_time" timestamp NOT NULL
,"planned_end_time" timestamp NOT NULL
,"actual_start_time" timestamp NOT NULL
,"actual_end_time" timestamp NOT NULL
,"planned_effort" decimal(10,2) NOT NULL
,"accounted_time" decimal(10,2) NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'change_workorder');

 drop foreign table if exists otrs_chat;
create FOREIGN TABLE otrs_chat("id" bigint NOT NULL
,"status" varchar(255) NOT NULL
,"requester_id" varchar(255) NOT NULL
,"requester_name" varchar(255) NOT NULL
,"requester_type" varchar(255) NOT NULL
,"target_type" varchar(255) NOT NULL
,"create_time" timestamp NOT NULL
,"change_time" timestamp
,"chat_channel_id" int
,"ticket_id" bigint)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'chat');

 drop foreign table if exists otrs_chat_channel;
create FOREIGN TABLE otrs_chat_channel("id" int NOT NULL
,"name" varchar(255) NOT NULL
,"group_id" int
,"valid_id" int NOT NULL
,"customer_interface" smallint
,"public_interface" smallint
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL
,"comments" varchar(255))SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'chat_channel');

 drop foreign table if exists otrs_chat_flag;
create FOREIGN TABLE otrs_chat_flag("id" bigint NOT NULL
,"chat_id" bigint NOT NULL
,"chat_key" varchar(255) NOT NULL
,"chat_value" varchar(255) NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'chat_flag');

 drop foreign table if exists otrs_chat_invite;
create FOREIGN TABLE otrs_chat_invite("id" int NOT NULL
,"chat_id" bigint NOT NULL
,"chatter_id" bigint NOT NULL
,"invitation_chat_id" smallint
,"create_time" timestamp NOT NULL
,"status" varchar(255))SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'chat_invite');

 drop foreign table if exists otrs_chat_message;
create FOREIGN TABLE otrs_chat_message("id" int NOT NULL
,"chat_id" bigint NOT NULL
,"chat_participant_id" bigint NOT NULL
,"message_text" text NOT NULL
,"system_generated" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"internal" smallint)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'chat_message');

 drop foreign table if exists otrs_chat_participant;
create FOREIGN TABLE otrs_chat_participant("id" bigint NOT NULL
,"chat_id" bigint NOT NULL
,"chatter_id" varchar(255) NOT NULL
,"chatter_name" varchar(255) NOT NULL
,"chatter_type" varchar(255) NOT NULL
,"chatter_active" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"permission_level" varchar(255)
,"invitation" smallint
,"monitoring_chat" smallint)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'chat_participant');

 drop foreign table if exists otrs_chat_video;
create FOREIGN TABLE otrs_chat_video("id" bigint NOT NULL
,"chat_id" bigint NOT NULL
,"requester_id" varchar(255) NOT NULL
,"requester_type" varchar(255) NOT NULL
,"target_id" varchar(255) NOT NULL
,"target_type" varchar(255) NOT NULL
,"signal_key" varchar(255) NOT NULL
,"signal_value" varchar(500) NOT NULL
,"signal_time" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'chat_video');

 drop foreign table if exists otrs_cip_allocate;
create FOREIGN TABLE otrs_cip_allocate("id" bigint NOT NULL
,"criticality" varchar(200) NOT NULL
,"impact" varchar(200) NOT NULL
,"priority_id" smallint NOT NULL
,"create_time" timestamp
,"create_by" int
,"change_time" timestamp
,"change_by" int)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'cip_allocate');

 drop foreign table if exists otrs_cloud_service_config;
create FOREIGN TABLE otrs_cloud_service_config("id" int NOT NULL
,"name" varchar(200) NOT NULL
,"config" text NOT NULL
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'cloud_service_config');

 drop foreign table if exists otrs_communication_channel;
create FOREIGN TABLE otrs_communication_channel("id" bigint NOT NULL
,"name" varchar(200) NOT NULL
,"module" varchar(200) NOT NULL
,"package_name" varchar(200) NOT NULL
,"channel_data" text NOT NULL
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'communication_channel');

 drop foreign table if exists otrs_communication_log;
create FOREIGN TABLE otrs_communication_log("id" bigint NOT NULL
,"insert_fingerprint" varchar(64)
,"transport" varchar(200) NOT NULL
,"direction" varchar(200) NOT NULL
,"status" varchar(200) NOT NULL
,"account_type" varchar(200)
,"account_id" varchar(200)
,"start_time" timestamp NOT NULL
,"end_time" timestamp)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'communication_log');

 drop foreign table if exists otrs_communication_log_object;
create FOREIGN TABLE otrs_communication_log_object("id" bigint NOT NULL
,"insert_fingerprint" varchar(64)
,"communication_id" bigint NOT NULL
,"object_type" varchar(50) NOT NULL
,"status" varchar(200) NOT NULL
,"start_time" timestamp NOT NULL
,"end_time" timestamp)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'communication_log_object');

 drop foreign table if exists otrs_communication_log_object_entry;
create FOREIGN TABLE otrs_communication_log_object_entry("id" bigint NOT NULL
,"communication_log_object_id" bigint NOT NULL
,"log_key" varchar(200) NOT NULL
,"log_value" varchar(500) NOT NULL
,"priority" varchar(50) NOT NULL
,"create_time" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'communication_log_object_entry');

 drop foreign table if exists otrs_communication_log_obj_lookup;
create FOREIGN TABLE otrs_communication_log_obj_lookup("id" bigint NOT NULL
,"communication_log_object_id" bigint NOT NULL
,"object_type" varchar(200) NOT NULL
,"object_id" bigint NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'communication_log_obj_lookup');

 drop foreign table if exists otrs_condition_action;
create FOREIGN TABLE otrs_condition_action("id" bigint NOT NULL
,"condition_id" bigint NOT NULL
,"action_number" int NOT NULL
,"object_id" smallint NOT NULL
,"attribute_id" smallint NOT NULL
,"operator_id" smallint NOT NULL
,"selector" varchar(20)
,"action_value" varchar(250))SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'condition_action');

 drop foreign table if exists otrs_condition_attribute;
create FOREIGN TABLE otrs_condition_attribute("id" smallint NOT NULL
,"name" varchar(250) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'condition_attribute');

 drop foreign table if exists otrs_condition_expression;
create FOREIGN TABLE otrs_condition_expression("id" bigint NOT NULL
,"condition_id" bigint NOT NULL
,"object_id" smallint NOT NULL
,"attribute_id" smallint NOT NULL
,"operator_id" smallint NOT NULL
,"selector" varchar(20)
,"compare_value" varchar(250))SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'condition_expression');

 drop foreign table if exists otrs_condition_object;
create FOREIGN TABLE otrs_condition_object("id" smallint NOT NULL
,"name" varchar(250) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'condition_object');

 drop foreign table if exists otrs_condition_operator;
create FOREIGN TABLE otrs_condition_operator("id" smallint NOT NULL
,"name" varchar(250) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'condition_operator');

 drop foreign table if exists otrs_configitem;
create FOREIGN TABLE otrs_configitem("id" bigint NOT NULL
,"configitem_number" varchar(100) NOT NULL
,"class_id" int NOT NULL
,"last_version_id" bigint
,"cur_depl_state_id" int
,"cur_inci_state_id" int
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'configitem');

 drop foreign table if exists otrs_configitem_counter;
create FOREIGN TABLE otrs_configitem_counter("class_id" int NOT NULL
,"counter_type" varchar(50) NOT NULL
,"counter" varchar(50) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'configitem_counter');

 drop foreign table if exists otrs_configitem_definition;
create FOREIGN TABLE otrs_configitem_definition("id" int NOT NULL
,"class_id" int NOT NULL
,"configitem_definition" text NOT NULL
,"version" int NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'configitem_definition');

 drop foreign table if exists otrs_configitem_history;
create FOREIGN TABLE otrs_configitem_history("id" bigint NOT NULL
,"configitem_id" bigint NOT NULL
,"type_id" int NOT NULL
,"content" varchar(255) NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'configitem_history');

 drop foreign table if exists otrs_configitem_history_type;
create FOREIGN TABLE otrs_configitem_history_type("id" int NOT NULL
,"name" varchar(255) NOT NULL
,"comments" varchar(255)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp
,"change_by" int)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'configitem_history_type');

 drop foreign table if exists otrs_configitem_version;
create FOREIGN TABLE otrs_configitem_version("id" bigint NOT NULL
,"configitem_id" bigint NOT NULL
,"name" varchar(250) NOT NULL
,"definition_id" int NOT NULL
,"depl_state_id" int NOT NULL
,"inci_state_id" int
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'configitem_version');

 drop foreign table if exists otrs_customer_company;
create FOREIGN TABLE otrs_customer_company("customer_id" varchar(150) NOT NULL
,"name" varchar(200) NOT NULL
,"street" varchar(200)
,"zip" varchar(200)
,"city" varchar(200)
,"country" varchar(200)
,"url" varchar(200)
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'customer_company');

 drop foreign table if exists otrs_customer_preferences;
create FOREIGN TABLE otrs_customer_preferences("user_id" varchar(250) NOT NULL
,"preferences_key" varchar(150) NOT NULL
,"preferences_value" varchar(250))SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'customer_preferences');

 drop foreign table if exists otrs_customer_user;
create FOREIGN TABLE otrs_customer_user("id" int NOT NULL
,"login" varchar(200) NOT NULL
,"email" varchar(150) NOT NULL
,"customer_id" varchar(150) NOT NULL
,"pw" varchar(128)
,"title" varchar(50)
,"first_name" varchar(100) NOT NULL
,"last_name" varchar(100) NOT NULL
,"phone" varchar(150)
,"fax" varchar(150)
,"mobile" varchar(150)
,"street" varchar(150)
,"zip" varchar(200)
,"city" varchar(200)
,"country" varchar(200)
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'customer_user');

 drop foreign table if exists otrs_customer_user_customer;
create FOREIGN TABLE otrs_customer_user_customer("user_id" varchar(100) NOT NULL
,"customer_id" varchar(150) NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'customer_user_customer');

 drop foreign table if exists otrs_custom_page;
create FOREIGN TABLE otrs_custom_page("id" int NOT NULL
,"internal_title" varchar(200) NOT NULL
,"slug" varchar(200) NOT NULL
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'custom_page');

 drop foreign table if exists otrs_custom_page_content;
create FOREIGN TABLE otrs_custom_page_content("id" int NOT NULL
,"custom_page_id" int NOT NULL
,"title" text NOT NULL
,"content" varchar(500) NOT NULL
,"language" varchar(60) NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'custom_page_content');

 drop foreign table if exists otrs_dynamic_field;
create FOREIGN TABLE otrs_dynamic_field("id" int NOT NULL
,"internal_field" smallint NOT NULL
,"name" varchar(200) NOT NULL
,"label" varchar(200) NOT NULL
,"field_order" int NOT NULL
,"field_type" varchar(200) NOT NULL
,"object_type" varchar(100) NOT NULL
,"config" text
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'dynamic_field');

 drop foreign table if exists otrs_dynamic_field_obj_id_name;
create FOREIGN TABLE otrs_dynamic_field_obj_id_name("object_id" int NOT NULL
,"object_name" varchar(200) NOT NULL
,"object_type" varchar(100) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'dynamic_field_obj_id_name');

 drop foreign table if exists otrs_dynamic_field_value;
create FOREIGN TABLE otrs_dynamic_field_value("id" int NOT NULL
,"field_id" int NOT NULL
,"object_id" bigint NOT NULL
,"value_text" text
,"value_date" timestamp
,"value_int" bigint)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'dynamic_field_value');

 drop foreign table if exists otrs_external_frontend_config;
create FOREIGN TABLE otrs_external_frontend_config("config_key" varchar(160) NOT NULL
,"config_value" text
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'external_frontend_config');

 drop foreign table if exists otrs_faq_attachment;
create FOREIGN TABLE otrs_faq_attachment("id" bigint NOT NULL
,"faq_id" bigint NOT NULL
,"filename" varchar(250)
,"content_size" varchar(30)
,"content_type" varchar(250)
,"content" text NOT NULL
,"inlineattachment" smallint NOT NULL
,"created" timestamp NOT NULL
,"created_by" int NOT NULL
,"changed" timestamp NOT NULL
,"changed_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'faq_attachment');

 drop foreign table if exists otrs_faq_category;
create FOREIGN TABLE otrs_faq_category("id" int NOT NULL
,"parent_id" int NOT NULL
,"name" varchar(200) NOT NULL
,"comments" varchar(200)
,"valid_id" smallint NOT NULL
,"created" timestamp NOT NULL
,"created_by" int NOT NULL
,"changed" timestamp NOT NULL
,"changed_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'faq_category');

 drop foreign table if exists otrs_faq_category_group;
create FOREIGN TABLE otrs_faq_category_group("id" int NOT NULL
,"category_id" int NOT NULL
,"group_id" int NOT NULL
,"created" timestamp
,"created_by" int
,"changed" timestamp
,"changed_by" int)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'faq_category_group');

 drop foreign table if exists otrs_faq_history;
create FOREIGN TABLE otrs_faq_history("id" int NOT NULL
,"name" varchar(200) NOT NULL
,"item_id" int NOT NULL
,"created" timestamp NOT NULL
,"created_by" int NOT NULL
,"changed" timestamp NOT NULL
,"changed_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'faq_history');

 drop foreign table if exists otrs_faq_item;
create FOREIGN TABLE otrs_faq_item("id" int NOT NULL
,"f_number" varchar(200) NOT NULL
,"f_subject" varchar(200)
,"f_name" varchar(200) NOT NULL
,"f_language_id" smallint NOT NULL
,"state_id" smallint NOT NULL
,"category_id" smallint NOT NULL
,"approved" smallint NOT NULL
,"valid_id" smallint NOT NULL
,"content_type" varchar(250) NOT NULL
,"f_keywords" text
,"f_field1" text
,"f_field2" text
,"f_field3" text
,"f_field4" text
,"f_field5" text
,"f_field6" text
,"created" timestamp NOT NULL
,"created_by" int NOT NULL
,"changed" timestamp NOT NULL
,"changed_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'faq_item');

 drop foreign table if exists otrs_faq_language;
create FOREIGN TABLE otrs_faq_language("id" smallint NOT NULL
,"name" varchar(200) NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'faq_language');

 drop foreign table if exists otrs_faq_log;
create FOREIGN TABLE otrs_faq_log("id" bigint NOT NULL
,"item_id" int NOT NULL
,"interface" varchar(80) NOT NULL
,"ip" varchar(200)
,"user_agent" varchar(200)
,"created" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'faq_log');

 drop foreign table if exists otrs_faq_state;
create FOREIGN TABLE otrs_faq_state("id" smallint NOT NULL
,"name" varchar(200) NOT NULL
,"type_id" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'faq_state');

 drop foreign table if exists otrs_faq_state_type;
create FOREIGN TABLE otrs_faq_state_type("id" smallint NOT NULL
,"name" varchar(200) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'faq_state_type');

 drop foreign table if exists otrs_faq_voting;
create FOREIGN TABLE otrs_faq_voting("id" bigint NOT NULL
,"created_by" varchar(200) NOT NULL
,"item_id" int NOT NULL
,"interface" varchar(80)
,"ip" varchar(50)
,"rate" int NOT NULL
,"created" timestamp)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'faq_voting');

 drop foreign table if exists otrs_follow_up_possible;
create FOREIGN TABLE otrs_follow_up_possible("id" smallint NOT NULL
,"name" varchar(200) NOT NULL
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'follow_up_possible');

 drop foreign table if exists otrs_form_draft;
create FOREIGN TABLE otrs_form_draft("id" int NOT NULL
,"object_type" varchar(100) NOT NULL
,"object_id" int NOT NULL
,"action" varchar(200) NOT NULL
,"title" varchar(255)
,"content" text NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'form_draft');

 drop foreign table if exists otrs_general_catalog;
create FOREIGN TABLE otrs_general_catalog("id" int NOT NULL
,"general_catalog_class" varchar(100) NOT NULL
,"name" varchar(100) NOT NULL
,"valid_id" smallint NOT NULL
,"comments" varchar(200)
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'general_catalog');

 drop foreign table if exists otrs_general_catalog_preferences;
create FOREIGN TABLE otrs_general_catalog_preferences("general_catalog_id" int NOT NULL
,"pref_key" varchar(255) NOT NULL
,"pref_value" varchar(255))SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'general_catalog_preferences');

 drop foreign table if exists otrs_generic_agent_jobs;
create FOREIGN TABLE otrs_generic_agent_jobs("job_name" varchar(200) NOT NULL
,"job_key" varchar(200) NOT NULL
,"job_value" varchar(200)
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'generic_agent_jobs');

 drop foreign table if exists otrs_gi_debugger_entry;
create FOREIGN TABLE otrs_gi_debugger_entry("id" bigint NOT NULL
,"communication_id" varchar(32) NOT NULL
,"communication_type" varchar(50) NOT NULL
,"remote_ip" varchar(50)
,"webservice_id" int NOT NULL
,"create_time" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'gi_debugger_entry');

 drop foreign table if exists otrs_gi_debugger_entry_content;
create FOREIGN TABLE otrs_gi_debugger_entry_content("id" bigint NOT NULL
,"gi_debugger_entry_id" bigint NOT NULL
,"debug_level" varchar(50) NOT NULL
,"subject" varchar(255) NOT NULL
,"content" text
,"create_time" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'gi_debugger_entry_content');

 drop foreign table if exists otrs_gi_webservice_config;
create FOREIGN TABLE otrs_gi_webservice_config("id" int NOT NULL
,"name" varchar(200) NOT NULL
,"config" text NOT NULL
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'gi_webservice_config');

 drop foreign table if exists otrs_gi_webservice_config_history;
create FOREIGN TABLE otrs_gi_webservice_config_history("id" bigint NOT NULL
,"config_id" int NOT NULL
,"config" text NOT NULL
,"config_md5" varchar(32) NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'gi_webservice_config_history');

 drop foreign table if exists otrs_groups_table;
create FOREIGN TABLE otrs_groups_table("id" int NOT NULL
,"name" varchar(200) NOT NULL
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'groups_table');

 drop foreign table if exists otrs_group_customer;
create FOREIGN TABLE otrs_group_customer("customer_id" varchar(150) NOT NULL
,"group_id" int NOT NULL
,"permission_key" varchar(20) NOT NULL
,"permission_value" smallint NOT NULL
,"permission_context" varchar(100) NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'group_customer');

 drop foreign table if exists otrs_group_customer_user;
create FOREIGN TABLE otrs_group_customer_user("user_id" varchar(100) NOT NULL
,"group_id" int NOT NULL
,"permission_key" varchar(20) NOT NULL
,"permission_value" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'group_customer_user');

 drop foreign table if exists otrs_group_role;
create FOREIGN TABLE otrs_group_role("role_id" int NOT NULL
,"group_id" int NOT NULL
,"permission_key" varchar(20) NOT NULL
,"permission_value" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'group_role');

 drop foreign table if exists otrs_group_user;
create FOREIGN TABLE otrs_group_user("user_id" int NOT NULL
,"group_id" int NOT NULL
,"permission_key" varchar(20) NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'group_user');

 drop foreign table if exists otrs_imexport_format;
create FOREIGN TABLE otrs_imexport_format("id" bigint NOT NULL
,"template_id" bigint NOT NULL
,"data_key" varchar(100) NOT NULL
,"data_value" varchar(200) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'imexport_format');

 drop foreign table if exists otrs_imexport_mapping;
create FOREIGN TABLE otrs_imexport_mapping("id" bigint NOT NULL
,"template_id" bigint NOT NULL
,"position" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'imexport_mapping');

 drop foreign table if exists otrs_imexport_mapping_format;
create FOREIGN TABLE otrs_imexport_mapping_format("id" bigint NOT NULL
,"mapping_id" bigint NOT NULL
,"data_key" varchar(100) NOT NULL
,"data_value" varchar(200) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'imexport_mapping_format');

 drop foreign table if exists otrs_imexport_mapping_object;
create FOREIGN TABLE otrs_imexport_mapping_object("id" bigint NOT NULL
,"mapping_id" bigint NOT NULL
,"data_key" varchar(100) NOT NULL
,"data_value" varchar(200) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'imexport_mapping_object');

 drop foreign table if exists otrs_imexport_object;
create FOREIGN TABLE otrs_imexport_object("id" bigint NOT NULL
,"template_id" bigint NOT NULL
,"data_key" varchar(100) NOT NULL
,"data_value" varchar(200) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'imexport_object');

 drop foreign table if exists otrs_imexport_search;
create FOREIGN TABLE otrs_imexport_search("id" bigint NOT NULL
,"template_id" bigint NOT NULL
,"data_key" varchar(100) NOT NULL
,"data_value" varchar(200) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'imexport_search');

 drop foreign table if exists otrs_imexport_template;
create FOREIGN TABLE otrs_imexport_template("id" bigint NOT NULL
,"imexport_object" varchar(100) NOT NULL
,"imexport_format" varchar(100) NOT NULL
,"name" varchar(100) NOT NULL
,"valid_id" smallint NOT NULL
,"comments" varchar(200)
,"create_time" timestamp
,"create_by" int
,"change_time" timestamp
,"change_by" int)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'imexport_template');

 drop foreign table if exists otrs_link_object;
create FOREIGN TABLE otrs_link_object("id" smallint NOT NULL
,"name" varchar(100) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'link_object');

 drop foreign table if exists otrs_link_relation;
create FOREIGN TABLE otrs_link_relation("source_object_id" smallint NOT NULL
,"source_key" varchar(50) NOT NULL
,"target_object_id" smallint NOT NULL
,"target_key" varchar(50) NOT NULL
,"type_id" smallint NOT NULL
,"state_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'link_relation');

 drop foreign table if exists otrs_link_state;
create FOREIGN TABLE otrs_link_state("id" smallint NOT NULL
,"name" varchar(50) NOT NULL
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'link_state');

 drop foreign table if exists otrs_link_type;
create FOREIGN TABLE otrs_link_type("id" smallint NOT NULL
,"name" varchar(50) NOT NULL
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'link_type');

 drop foreign table if exists otrs_mail_account;
create FOREIGN TABLE otrs_mail_account("id" int NOT NULL
,"login" varchar(200) NOT NULL
,"pw" varchar(200) NOT NULL
,"host" varchar(200) NOT NULL
,"account_type" varchar(20) NOT NULL
,"queue_id" int NOT NULL
,"trusted" smallint NOT NULL
,"imap_folder" varchar(250)
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'mail_account');

 drop foreign table if exists otrs_mail_queue;
create FOREIGN TABLE otrs_mail_queue("id" bigint NOT NULL
,"insert_fingerprint" varchar(64)
,"article_id" bigint
,"attempts" int NOT NULL
,"sender" varchar(200)
,"recipient" varchar(500) NOT NULL
,"raw_message" text NOT NULL
,"due_time" timestamp
,"last_smtp_code" int
,"last_smtp_message" varchar(500)
,"create_time" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'mail_queue');

 drop foreign table if exists otrs_notification_event;
create FOREIGN TABLE otrs_notification_event("id" int NOT NULL
,"name" varchar(200) NOT NULL
,"valid_id" smallint NOT NULL
,"comments" varchar(250)
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'notification_event');

 drop foreign table if exists otrs_notification_event_item;
create FOREIGN TABLE otrs_notification_event_item("notification_id" int NOT NULL
,"event_key" varchar(200) NOT NULL
,"event_value" varchar(200) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'notification_event_item');

 drop foreign table if exists otrs_notification_event_message;
create FOREIGN TABLE otrs_notification_event_message("id" int NOT NULL
,"notification_id" int NOT NULL
,"subject" varchar(200) NOT NULL
,"text" text NOT NULL
,"content_type" varchar(250) NOT NULL
,"language" varchar(60) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'notification_event_message');

 drop foreign table if exists otrs_notification_view;
create FOREIGN TABLE otrs_notification_view("id" int NOT NULL
,"notification_entity" varchar(32) NOT NULL
,"name" varchar(200) NOT NULL
,"comments" varchar(250)
,"subject" varchar(200) NOT NULL
,"body" varchar(500) NOT NULL
,"user_id" varchar(150) NOT NULL
,"object_type" varchar(50) NOT NULL
,"object_id" int NOT NULL
,"object_reference" varchar(200) NOT NULL
,"seen" smallint
,"create_time" timestamp NOT NULL
,"user_type" varchar(50) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'notification_view');

 drop foreign table if exists otrs_package_repository;
create FOREIGN TABLE otrs_package_repository("id" int NOT NULL
,"name" varchar(200) NOT NULL
,"version" varchar(250) NOT NULL
,"vendor" varchar(250) NOT NULL
,"install_status" varchar(250) NOT NULL
,"filename" varchar(250)
,"content_type" varchar(250)
,"content" text NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'package_repository');

 drop foreign table if exists otrs_personal_queues;
create FOREIGN TABLE otrs_personal_queues("user_id" int NOT NULL
,"queue_id" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'personal_queues');

 drop foreign table if exists otrs_personal_services;
create FOREIGN TABLE otrs_personal_services("user_id" int NOT NULL
,"service_id" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'personal_services');

 drop foreign table if exists otrs_pm_activity;
create FOREIGN TABLE otrs_pm_activity("id" int NOT NULL
,"entity_id" varchar(60) NOT NULL
,"name" varchar(200) NOT NULL
,"config" text NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL
,"activity_type" varchar(100) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'pm_activity');

 drop foreign table if exists otrs_pm_activity_dialog;
create FOREIGN TABLE otrs_pm_activity_dialog("id" int NOT NULL
,"entity_id" varchar(60) NOT NULL
,"name" varchar(200) NOT NULL
,"config" text NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'pm_activity_dialog');

 drop foreign table if exists otrs_pm_deployment;
create FOREIGN TABLE otrs_pm_deployment("id" int NOT NULL
,"uuid" varchar(36) NOT NULL
,"effective_value" text NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'pm_deployment');

 drop foreign table if exists otrs_pm_entity_sync;
create FOREIGN TABLE otrs_pm_entity_sync("entity_type" varchar(30) NOT NULL
,"entity_id" varchar(60) NOT NULL
,"sync_state" varchar(30) NOT NULL
,"create_time" timestamp NOT NULL
,"change_time" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'pm_entity_sync');

 drop foreign table if exists otrs_pm_process;
create FOREIGN TABLE otrs_pm_process("id" int NOT NULL
,"entity_id" varchar(60) NOT NULL
,"name" varchar(200) NOT NULL
,"state_entity_id" varchar(60) NOT NULL
,"layout" text NOT NULL
,"config" text NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'pm_process');

 drop foreign table if exists otrs_pm_sequence_flow;
create FOREIGN TABLE otrs_pm_sequence_flow("id" int NOT NULL
,"entity_id" varchar(60) NOT NULL
,"name" varchar(200) NOT NULL
,"config" text NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'pm_sequence_flow');

 drop foreign table if exists otrs_pm_sequence_flow_action;
create FOREIGN TABLE otrs_pm_sequence_flow_action("id" int NOT NULL
,"entity_id" varchar(60) NOT NULL
,"name" varchar(200) NOT NULL
,"config" text NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'pm_sequence_flow_action');

 drop foreign table if exists otrs_postmaster_filter;
create FOREIGN TABLE otrs_postmaster_filter("f_name" varchar(200) NOT NULL
,"f_stop" smallint
,"f_type" varchar(20) NOT NULL
,"f_key" varchar(200) NOT NULL
,"f_value" varchar(200) NOT NULL
,"f_not" smallint
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'postmaster_filter');

 drop foreign table if exists otrs_process_id;
create FOREIGN TABLE otrs_process_id("process_name" varchar(200) NOT NULL
,"process_id" varchar(200) NOT NULL
,"process_host" varchar(200) NOT NULL
,"process_create" int NOT NULL
,"process_change" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'process_id');

 drop foreign table if exists otrs_queue;
create FOREIGN TABLE otrs_queue("id" int NOT NULL
,"name" varchar(200) NOT NULL
,"group_id" int NOT NULL
,"unlock_timeout" int
,"first_response_time" int
,"first_response_notify" smallint
,"update_time" int
,"update_notify" smallint
,"solution_time" int
,"solution_notify" smallint
,"system_address_id" smallint NOT NULL
,"calendar_name" varchar(100)
,"default_sign_key" varchar(100)
,"salutation_id" smallint NOT NULL
,"signature_id" smallint NOT NULL
,"follow_up_id" smallint NOT NULL
,"follow_up_lock" smallint NOT NULL
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'queue');

 drop foreign table if exists otrs_queue_auto_response;
create FOREIGN TABLE otrs_queue_auto_response("id" int NOT NULL
,"queue_id" int NOT NULL
,"auto_response_id" int NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'queue_auto_response');

 drop foreign table if exists otrs_queue_preferences;
create FOREIGN TABLE otrs_queue_preferences("queue_id" int NOT NULL
,"preferences_key" varchar(150) NOT NULL
,"preferences_value" varchar(250))SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'queue_preferences');

 drop foreign table if exists otrs_queue_service;
create FOREIGN TABLE otrs_queue_service("queue_id" int NOT NULL
,"service_id" int NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'queue_service');

 drop foreign table if exists otrs_queue_sms_template;
create FOREIGN TABLE otrs_queue_sms_template("queue_id" int NOT NULL
,"sms_template_id" int NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'queue_sms_template');

 drop foreign table if exists otrs_queue_standard_template;
create FOREIGN TABLE otrs_queue_standard_template("queue_id" int NOT NULL
,"standard_template_id" int NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'queue_standard_template');

 drop foreign table if exists otrs_roles;
create FOREIGN TABLE otrs_roles("id" int NOT NULL
,"name" varchar(200) NOT NULL
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'roles');

 drop foreign table if exists otrs_role_user;
create FOREIGN TABLE otrs_role_user("user_id" int NOT NULL
,"role_id" int NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'role_user');

 drop foreign table if exists otrs_salutation;
create FOREIGN TABLE otrs_salutation("id" smallint NOT NULL
,"name" varchar(200) NOT NULL
,"text" text NOT NULL
,"content_type" varchar(250)
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'salutation');

 drop foreign table if exists otrs_scheduler_future_task;
create FOREIGN TABLE otrs_scheduler_future_task("id" bigint NOT NULL
,"ident" bigint NOT NULL
,"execution_time" timestamp NOT NULL
,"name" varchar(150)
,"task_type" varchar(150) NOT NULL
,"task_data" text NOT NULL
,"attempts" smallint NOT NULL
,"lock_key" bigint NOT NULL
,"lock_time" timestamp
,"create_time" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'scheduler_future_task');

 drop foreign table if exists otrs_scheduler_recurrent_task;
create FOREIGN TABLE otrs_scheduler_recurrent_task("id" bigint NOT NULL
,"name" varchar(150) NOT NULL
,"task_type" varchar(150) NOT NULL
,"last_execution_time" timestamp NOT NULL
,"last_worker_task_id" bigint
,"last_worker_status" smallint
,"last_worker_running_time" int
,"lock_key" bigint NOT NULL
,"lock_time" timestamp
,"create_time" timestamp NOT NULL
,"change_time" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'scheduler_recurrent_task');

 drop foreign table if exists otrs_scheduler_task;
create FOREIGN TABLE otrs_scheduler_task("id" bigint NOT NULL
,"ident" bigint NOT NULL
,"name" varchar(150)
,"task_type" varchar(150) NOT NULL
,"task_data" text NOT NULL
,"attempts" smallint NOT NULL
,"lock_key" bigint NOT NULL
,"lock_time" timestamp
,"lock_update_time" timestamp
,"create_time" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'scheduler_task');

 drop foreign table if exists otrs_sc_category;
create FOREIGN TABLE otrs_sc_category("id" int NOT NULL
,"title" varchar(200) NOT NULL
,"parent_id" int
,"language" varchar(60) NOT NULL
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'sc_category');

 drop foreign table if exists otrs_sc_item;
create FOREIGN TABLE otrs_sc_item("id" int NOT NULL
,"internal_title" varchar(200) NOT NULL
,"valid_id" smallint NOT NULL
,"usage_counter" bigint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'sc_item');

 drop foreign table if exists otrs_sc_item_content;
create FOREIGN TABLE otrs_sc_item_content("id" int NOT NULL
,"item_id" int NOT NULL
,"title" varchar(250)
,"text" varchar(250)
,"language" varchar(60) NOT NULL
,"link_location" varchar(250)
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'sc_item_content');

 drop foreign table if exists otrs_sc_item_content_category;
create FOREIGN TABLE otrs_sc_item_content_category("item_content_id" int NOT NULL
,"category_id" int NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'sc_item_content_category');

 drop foreign table if exists otrs_search_profile;
create FOREIGN TABLE otrs_search_profile("login" varchar(200) NOT NULL
,"profile_name" varchar(200) NOT NULL
,"profile_type" varchar(30) NOT NULL
,"profile_key" varchar(200) NOT NULL
,"profile_value" varchar(200))SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'search_profile');

 drop foreign table if exists otrs_search_state;
create FOREIGN TABLE otrs_search_state("id" bigint NOT NULL
,"document_type" varchar(255) NOT NULL
,"document_id" bigint NOT NULL
,"needs_rebuild" smallint NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'search_state');

 drop foreign table if exists otrs_service;
create FOREIGN TABLE otrs_service("id" int NOT NULL
,"name" varchar(200) NOT NULL
,"valid_id" smallint NOT NULL
,"comments" varchar(250)
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL
,"type_id" int
,"criticality" varchar(200))SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'service');

 drop foreign table if exists otrs_service_customer_user;
create FOREIGN TABLE otrs_service_customer_user("customer_user_login" varchar(200) NOT NULL
,"service_id" int NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'service_customer_user');

 drop foreign table if exists otrs_service_preferences;
create FOREIGN TABLE otrs_service_preferences("service_id" int NOT NULL
,"preferences_key" varchar(150) NOT NULL
,"preferences_value" varchar(250))SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'service_preferences');

 drop foreign table if exists otrs_service_sla;
create FOREIGN TABLE otrs_service_sla("service_id" int NOT NULL
,"sla_id" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'service_sla');

 drop foreign table if exists otrs_sessions;
create FOREIGN TABLE otrs_sessions("id" bigint NOT NULL
,"session_id" varchar(100) NOT NULL
,"data_key" varchar(100) NOT NULL
,"data_value" text
,"serialized" smallint NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'sessions');

 drop foreign table if exists otrs_signature;
create FOREIGN TABLE otrs_signature("id" smallint NOT NULL
,"name" varchar(200) NOT NULL
,"text" text NOT NULL
,"content_type" varchar(250)
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'signature');

 drop foreign table if exists otrs_sla;
create FOREIGN TABLE otrs_sla("id" int NOT NULL
,"name" varchar(200) NOT NULL
,"calendar_name" varchar(100)
,"first_response_time" int NOT NULL
,"first_response_notify" smallint
,"update_time" int NOT NULL
,"update_notify" smallint
,"solution_time" int NOT NULL
,"solution_notify" smallint
,"valid_id" smallint NOT NULL
,"comments" varchar(250)
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL
,"type_id" int
,"min_time_bet_incidents" int)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'sla');

 drop foreign table if exists otrs_sla_preferences;
create FOREIGN TABLE otrs_sla_preferences("sla_id" int NOT NULL
,"preferences_key" varchar(150) NOT NULL
,"preferences_value" varchar(250))SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'sla_preferences');

 drop foreign table if exists otrs_smime_signer_cert_relations;
create FOREIGN TABLE otrs_smime_signer_cert_relations("id" int NOT NULL
,"cert_hash" varchar(8) NOT NULL
,"cert_fingerprint" varchar(59) NOT NULL
,"ca_hash" varchar(8) NOT NULL
,"ca_fingerprint" varchar(59) NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'smime_signer_cert_relations');

 drop foreign table if exists otrs_sms_template;
create FOREIGN TABLE otrs_sms_template("id" int NOT NULL
,"name" varchar(200) NOT NULL
,"text" text
,"is_flash_message" smallint
,"template_type" varchar(100) NOT NULL
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'sms_template');

 drop foreign table if exists otrs_standard_attachment;
create FOREIGN TABLE otrs_standard_attachment("id" int NOT NULL
,"name" varchar(200) NOT NULL
,"content_type" varchar(250) NOT NULL
,"content" text NOT NULL
,"filename" varchar(250) NOT NULL
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'standard_attachment');

 drop foreign table if exists otrs_standard_template;
create FOREIGN TABLE otrs_standard_template("id" int NOT NULL
,"name" varchar(200) NOT NULL
,"text" text
,"content_type" varchar(250)
,"template_type" varchar(100) NOT NULL
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL
,"preselected_ticket_state_id" int
,"subject" varchar(200)
,"subject_method" varchar(100))SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'standard_template');

 drop foreign table if exists otrs_standard_template_attachment;
create FOREIGN TABLE otrs_standard_template_attachment("id" int NOT NULL
,"standard_attachment_id" int NOT NULL
,"standard_template_id" int NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'standard_template_attachment');

 drop foreign table if exists otrs_stats_report;
create FOREIGN TABLE otrs_stats_report("id" int NOT NULL
,"name" varchar(200) NOT NULL
,"config" text NOT NULL
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'stats_report');

 drop foreign table if exists otrs_sysconfig_default;
create FOREIGN TABLE otrs_sysconfig_default("id" int NOT NULL
,"name" varchar(250) NOT NULL
,"description" text NOT NULL
,"navigation" varchar(200) NOT NULL
,"is_invisible" smallint NOT NULL
,"is_readonly" smallint NOT NULL
,"is_required" smallint NOT NULL
,"is_valid" smallint NOT NULL
,"has_configlevel" smallint NOT NULL
,"user_modification_possible" smallint NOT NULL
,"user_modification_active" smallint NOT NULL
,"user_preferences_group" varchar(250)
,"xml_content_raw" text NOT NULL
,"xml_content_parsed" text NOT NULL
,"xml_filename" varchar(250) NOT NULL
,"effective_value" text NOT NULL
,"is_dirty" smallint NOT NULL
,"exclusive_lock_guid" varchar(32) NOT NULL
,"exclusive_lock_user_id" int
,"exclusive_lock_expiry_time" timestamp
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'sysconfig_default');

 drop foreign table if exists otrs_sysconfig_default_version;
create FOREIGN TABLE otrs_sysconfig_default_version("id" int NOT NULL
,"sysconfig_default_id" int
,"name" varchar(250) NOT NULL
,"description" text NOT NULL
,"navigation" varchar(200) NOT NULL
,"is_invisible" smallint NOT NULL
,"is_readonly" smallint NOT NULL
,"is_required" smallint NOT NULL
,"is_valid" smallint NOT NULL
,"has_configlevel" smallint NOT NULL
,"user_modification_possible" smallint NOT NULL
,"user_modification_active" smallint NOT NULL
,"user_preferences_group" varchar(250)
,"xml_content_raw" text NOT NULL
,"xml_content_parsed" text NOT NULL
,"xml_filename" varchar(250) NOT NULL
,"effective_value" text NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'sysconfig_default_version');

 drop foreign table if exists otrs_sysconfig_deployment;
create FOREIGN TABLE otrs_sysconfig_deployment("id" int NOT NULL
,"comments" varchar(250)
,"user_id" int
,"effective_value" text NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"uuid" varchar(36) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'sysconfig_deployment');

 drop foreign table if exists otrs_sysconfig_deployment_lock;
create FOREIGN TABLE otrs_sysconfig_deployment_lock("id" int NOT NULL
,"exclusive_lock_guid" varchar(32)
,"exclusive_lock_user_id" int
,"exclusive_lock_expiry_time" timestamp)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'sysconfig_deployment_lock');

 drop foreign table if exists otrs_sysconfig_modified;
create FOREIGN TABLE otrs_sysconfig_modified("id" int NOT NULL
,"sysconfig_default_id" int NOT NULL
,"name" varchar(250) NOT NULL
,"user_id" int
,"is_valid" smallint NOT NULL
,"user_modification_active" smallint NOT NULL
,"effective_value" text NOT NULL
,"is_dirty" smallint NOT NULL
,"reset_to_default" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'sysconfig_modified');

 drop foreign table if exists otrs_sysconfig_modified_version;
create FOREIGN TABLE otrs_sysconfig_modified_version("id" int NOT NULL
,"sysconfig_default_version_id" int NOT NULL
,"name" varchar(250) NOT NULL
,"user_id" int
,"is_valid" smallint NOT NULL
,"user_modification_active" smallint NOT NULL
,"effective_value" text NOT NULL
,"reset_to_default" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'sysconfig_modified_version');

 drop foreign table if exists otrs_system_address;
create FOREIGN TABLE otrs_system_address("id" smallint NOT NULL
,"value0" varchar(200) NOT NULL
,"value1" varchar(200) NOT NULL
,"value2" varchar(200)
,"value3" varchar(200)
,"queue_id" int NOT NULL
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'system_address');

 drop foreign table if exists otrs_system_data;
create FOREIGN TABLE otrs_system_data("data_key" varchar(160) NOT NULL
,"data_value" text
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'system_data');

 drop foreign table if exists otrs_system_maintenance;
create FOREIGN TABLE otrs_system_maintenance("id" int NOT NULL
,"start_date" int NOT NULL
,"stop_date" int NOT NULL
,"comments" varchar(250) NOT NULL
,"login_message" varchar(250)
,"show_login_message" smallint
,"notify_message" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'system_maintenance');

 drop foreign table if exists otrs_ticket;
create FOREIGN TABLE otrs_ticket("id" bigint NOT NULL
,"tn" varchar(50) NOT NULL
,"title" varchar(255)
,"queue_id" int NOT NULL
,"ticket_lock_id" smallint NOT NULL
,"type_id" smallint
,"service_id" int
,"sla_id" int
,"user_id" int NOT NULL
,"responsible_user_id" int NOT NULL
,"ticket_priority_id" smallint NOT NULL
,"ticket_state_id" smallint NOT NULL
,"customer_id" varchar(150)
,"customer_user_id" varchar(250)
,"timeout" int NOT NULL
,"until_time" int NOT NULL
,"escalation_time" int NOT NULL
,"escalation_update_time" int NOT NULL
,"escalation_response_time" int NOT NULL
,"escalation_solution_time" int NOT NULL
,"archive_flag" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'ticket');

 drop foreign table if exists otrs_ticket_customer_flag;
create FOREIGN TABLE otrs_ticket_customer_flag("ticket_id" bigint NOT NULL
,"ticket_key" varchar(50) NOT NULL
,"ticket_value" varchar(50)
,"create_time" timestamp NOT NULL
,"login" varchar(200) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'ticket_customer_flag');

 drop foreign table if exists otrs_ticket_flag;
create FOREIGN TABLE otrs_ticket_flag("ticket_id" bigint NOT NULL
,"ticket_key" varchar(50) NOT NULL
,"ticket_value" varchar(50)
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'ticket_flag');

 drop foreign table if exists otrs_ticket_history;
create FOREIGN TABLE otrs_ticket_history("id" bigint NOT NULL
,"name" varchar(200) NOT NULL
,"history_type_id" smallint NOT NULL
,"ticket_id" bigint NOT NULL
,"article_id" bigint
,"type_id" smallint NOT NULL
,"queue_id" int NOT NULL
,"owner_id" int NOT NULL
,"priority_id" smallint NOT NULL
,"state_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'ticket_history');

 drop foreign table if exists otrs_ticket_history_type;
create FOREIGN TABLE otrs_ticket_history_type("id" smallint NOT NULL
,"name" varchar(200) NOT NULL
,"comments" varchar(250)
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'ticket_history_type');

 drop foreign table if exists otrs_ticket_index;
create FOREIGN TABLE otrs_ticket_index("ticket_id" bigint NOT NULL
,"queue_id" int NOT NULL
,"queue" varchar(200) NOT NULL
,"group_id" int NOT NULL
,"s_lock" varchar(200) NOT NULL
,"s_state" varchar(200) NOT NULL
,"create_time" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'ticket_index');

 drop foreign table if exists otrs_ticket_lock_index;
create FOREIGN TABLE otrs_ticket_lock_index("ticket_id" bigint NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'ticket_lock_index');

 drop foreign table if exists otrs_ticket_lock_type;
create FOREIGN TABLE otrs_ticket_lock_type("id" smallint NOT NULL
,"name" varchar(200) NOT NULL
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'ticket_lock_type');

 drop foreign table if exists otrs_ticket_loop_protection;
create FOREIGN TABLE otrs_ticket_loop_protection("sent_to" varchar(250) NOT NULL
,"sent_date" varchar(150) NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'ticket_loop_protection');

 drop foreign table if exists otrs_ticket_number_counter;
create FOREIGN TABLE otrs_ticket_number_counter("id" bigint NOT NULL
,"counter" bigint NOT NULL
,"counter_uid" varchar(32) NOT NULL
,"create_time" timestamp)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'ticket_number_counter');

 drop foreign table if exists otrs_ticket_priority;
create FOREIGN TABLE otrs_ticket_priority("id" smallint NOT NULL
,"name" varchar(200) NOT NULL
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'ticket_priority');

 drop foreign table if exists otrs_ticket_state;
create FOREIGN TABLE otrs_ticket_state("id" smallint NOT NULL
,"name" varchar(200) NOT NULL
,"comments" varchar(250)
,"type_id" smallint NOT NULL
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'ticket_state');

 drop foreign table if exists otrs_ticket_state_type;
create FOREIGN TABLE otrs_ticket_state_type("id" smallint NOT NULL
,"name" varchar(200) NOT NULL
,"comments" varchar(250)
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'ticket_state_type');

 drop foreign table if exists otrs_ticket_type;
create FOREIGN TABLE otrs_ticket_type("id" smallint NOT NULL
,"name" varchar(200) NOT NULL
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'ticket_type');

 drop foreign table if exists otrs_ticket_type_service;
create FOREIGN TABLE otrs_ticket_type_service("ticket_type_id" smallint NOT NULL
,"service_id" int NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'ticket_type_service');

 drop foreign table if exists otrs_ticket_watcher;
create FOREIGN TABLE otrs_ticket_watcher("ticket_id" bigint NOT NULL
,"user_id" int NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'ticket_watcher');

 drop foreign table if exists otrs_time_accounting;
create FOREIGN TABLE otrs_time_accounting("id" bigint NOT NULL
,"ticket_id" bigint NOT NULL
,"article_id" bigint
,"time_unit" decimal(10,2) NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'time_accounting');

 drop foreign table if exists otrs_users;
create FOREIGN TABLE otrs_users("id" int NOT NULL
,"login" varchar(200) NOT NULL
,"pw" varchar(128) NOT NULL
,"title" varchar(50)
,"first_name" varchar(100) NOT NULL
,"last_name" varchar(100) NOT NULL
,"valid_id" smallint NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'users');

 drop foreign table if exists otrs_user_preferences;
create FOREIGN TABLE otrs_user_preferences("user_id" int NOT NULL
,"preferences_key" varchar(150) NOT NULL
,"preferences_value" text)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'user_preferences');

 drop foreign table if exists otrs_valid;
create FOREIGN TABLE otrs_valid("id" smallint NOT NULL
,"name" varchar(200) NOT NULL
,"create_time" timestamp NOT NULL
,"create_by" int NOT NULL
,"change_time" timestamp NOT NULL
,"change_by" int NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'valid');

 drop foreign table if exists otrs_virtual_fs;
create FOREIGN TABLE otrs_virtual_fs("id" bigint NOT NULL
,"filename" text NOT NULL
,"backend" varchar(60) NOT NULL
,"backend_key" varchar(160) NOT NULL
,"create_time" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'virtual_fs');

 drop foreign table if exists otrs_virtual_fs_db;
create FOREIGN TABLE otrs_virtual_fs_db("id" bigint NOT NULL
,"filename" text NOT NULL
,"content" text
,"create_time" timestamp NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'virtual_fs_db');

 drop foreign table if exists otrs_virtual_fs_preferences;
create FOREIGN TABLE otrs_virtual_fs_preferences("virtual_fs_id" bigint NOT NULL
,"preferences_key" varchar(150) NOT NULL
,"preferences_value" text)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'virtual_fs_preferences');

 drop foreign table if exists otrs_web_upload_cache;
create FOREIGN TABLE otrs_web_upload_cache("form_id" varchar(250)
,"filename" varchar(250)
,"content_id" varchar(250)
,"content_size" varchar(30)
,"content_type" varchar(250)
,"disposition" varchar(15)
,"content" text NOT NULL
,"create_time_unix" bigint NOT NULL)SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'web_upload_cache');

 drop foreign table if exists otrs_xml_storage;
create FOREIGN TABLE otrs_xml_storage("xml_type" varchar(200) NOT NULL
,"xml_key" varchar(250) NOT NULL
,"xml_content_key" varchar(250) NOT NULL
,"xml_content_value" varchar(500))SERVER mysql_fdw_otrs OPTIONS (dbname 'otrs', table_name 'xml_storage');
