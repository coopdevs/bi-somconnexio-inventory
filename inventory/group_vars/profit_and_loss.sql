
create or replace view v_apuntes_contables as
  select
    aag."name" as group_name_lvl2, coalesce(aagp."name",aag."name")  as group_name_lvl1, aag.complete_name as group_complete_name
    , aaa.name as analyic_account_name,  aaa.create_date, rp."name"  as partner_name
    ,am.name as asiento_contable, am.ref, am.date
    , rp.display_name as empresa
    ,aa.name , aa.code, aa.internal_type, aa.internal_group
    , aat."name"  as account_type
    , ag.name as anal_acc_group_name_lvl1, ag1."name"  as anal_acc_group_name_lvl2 , ag2.name  as anal_acc_group_name_lvl3
    ,aml.name as move_line_name, aml.quantity , aml.debit , aml.credit , aml.balance  --si balance < 0 es bo
  from odoo_account_move_line aml
    join odoo_account_move am on aml.move_id =am.id
    left join odoo_res_partner rp  on aml.partner_id =rp.id
    join odoo_account_account aa on aml.account_id =aa.id
    join odoo_account_account_type aat on aa.user_type_id =aat.id
    join odoo_account_group ag on aa.group_id =ag.id
    left join odoo_account_group ag1 on ag.parent_id =ag1.id
    left join odoo_account_group ag2 on ag1.parent_id =ag2.id
    left join odoo_account_analytic_account aaa on aml.analytic_account_id =aaa.id
    left join odoo_account_analytic_group aag on aaa.group_id =aag.id
    left join odoo_account_analytic_group aagp on aag.parent_id=aagp.id;



create or replace  view v_apuntes_contables_year_acum as
select group_name_lvl2,group_name_lvl1,group_complete_name,analyic_account_name,create_date,partner_name,asiento_contable,"ref",d.data,empresa,"name",code,internal_type,internal_group,account_type,anal_acc_group_name_lvl1,anal_acc_group_name_lvl2,anal_acc_group_name_lvl3,move_line_name
,sum(quantity) as quantity,sum(debit) as debit, sum(credit) as credit,sum(balance) as balance
from data d
left join v_apuntes_contables a on date_trunc('year', d.data) =date_trunc('year', a.date) and d.data>=a.date
where date_part('day', d.data)=d.dies_mes
and d.data>='20200101'
group by group_name_lvl2,group_name_lvl1,group_complete_name,analyic_account_name,create_date,partner_name,asiento_contable,"ref",d.data,empresa,"name",code,internal_type,internal_group,account_type,anal_acc_group_name_lvl1,anal_acc_group_name_lvl2,anal_acc_group_name_lvl3,move_line_name;


create or replace view v_apuntes_contables_agr as
select date
,anal_acc_group_name_lvl3 as cuenta_contable_lvl1
, anal_acc_group_name_lvl3||'-'|| anal_acc_group_name_lvl2 as cuenta_contable_lvl2
, anal_acc_group_name_lvl3||'-'|| anal_acc_group_name_lvl2||'-'|| anal_acc_group_name_lvl1  as cuenta_contable_lvl3
, anal_acc_group_name_lvl3||'-'|| anal_acc_group_name_lvl2||'-'|| anal_acc_group_name_lvl1||'-'|| code  as cuenta_contable_detalle
,sum(debit)as debit, sum(credit) as credit, sum(balance) as balance
from v_apuntes_contables d
where anal_acc_group_name_lvl3 in ('Compras y gastos','Ventas e ingresos')
group by  d.date, anal_acc_group_name_lvl3, anal_acc_group_name_lvl2, anal_acc_group_name_lvl1, code ;
