drop view if exists opencell_contract_lines CASCADE;
create view opencell_contract_lines as
SELECT bs.code as Subscriptioncode,
bs.description as Subscription,
bs.status as Subscriptionstatus,
        bsi.id as BSIID,
        bsi.subscription_id as SubscriptionID,
bsi.code as Servicecode,
        bsi.subscription_date as Serviceactivationdate,
        bsi.termination_date as Serviceterminationdate,
        bs.termination_date,
        bua.id,
        bae.code AS UserAccountcode,
        ba.billing_cycle,
        ba.trading_country_id,
        ba.trading_language_id,
        ba.id AS billing_account_id,
        bc.code AS BillingCycleCode,
        ac.country_code AS Pais,
al.language_code AS Idioma,
        bs.billing_cycle as Subsbillingcycle,
        bs.electronic_billing as EBilling,
        bae.description as BUAdesc
from opencell_billing_subscription bs
INNER JOIN opencell_billing_service_instance bsi ON bsi.subscription_id = bs.id
    INNER JOIN opencell_billing_user_account bua ON bua.id = bs.user_account_id
  INNER JOIN opencell_account_entity bae on bua.id = bae.id
    INNER JOIN opencell_billing_billing_account ba ON ba.id = bua.billing_account_id
    INNER JOIN opencell_billing_cycle bc ON bc.id = ba.billing_cycle
    INNER JOIN opencell_billing_trading_country btc ON ba.trading_country_id = btc.id
    INNER JOIN opencell_billing_trading_language btl ON ba.trading_language_id = btl.id
    INNER JOIN opencell_adm_country ac on ac.id = btc.country_id
    INNER JOIN opencell_adm_language al on al.id = btl.language_id
where  bsi.subscription_date<bsi.termination_date or bsi.termination_date is null  --eliminar les caducades
  and bsi.code not in ('ISCAT_SC_SUBSCRIPTION_BA_BORDA_OPEX', 'SE_SC_REC_BA_IP_fix',
    'SE_SC_REC_INT_PACK_T_100', 'SE_SC_REC_INT_PACK_T_300', 'SE_SC_REC_INT_PACK_T_600') -- productes a descartar
;


drop view if exists odoo_contract_lines CASCADE;
create view odoo_contract_lines as
select opp.default_code, occ.code, ocl.date_start, ocl.date_end, occ.name
from odoo_contract_contract occ
join odoo_contract_line ocl on occ.id=ocl.contract_id
join odoo_product_product opp on ocl.product_id =opp.id
where (ocl.date_start<ocl.date_end or ocl.date_end is null) --eliminar les caducades
and opp.default_code is not null  --eliminar les que no tenen producte
and opp.default_code not like 'CH_SC_OSO%'
and opp.default_code not in ('ISCAT_SC_SUBSCRIPTION_BA_BORDA_OPEX', 'SE_SC_REC_BA_IP_fix',
  'SE_SC_REC_INT_PACK_T_100', 'SE_SC_REC_INT_PACK_T_300', 'SE_SC_REC_INT_PACK_T_600') -- productes a descartar
;


/*
drop table "external".validacio_factures;
create table "external".validacio_factures as
select
case when o.code is null then 'FALTA ODOO' else 'CORRECTE' end as flag_existeix_odoo
,case when c.subscriptioncode is null then 'FALTA OPENCELL' else 'CORRECTE' end as flag_existeix_opencell
,case when coalesce(o.default_code,'xx')<>c.servicecode then 'PRODUCTE DIFERENT' else 'CORRECTE' end as flag_validacio_producte
,case when coalesce(o.date_end, '99991231')<>coalesce(c.Serviceterminationdate,'99991231') then 'DATA FI DIFERENT' else 'CORRECTE' end as flag_validacio_data_fi
,case when o.date_start<>c.Serviceactivationdate then 'DATA INICI DIFERENT' else 'CORRECTE' end as flag_validacio_data_inici
,default_code as codi_producte_odoo, code as codi_contracte_odoo, date_start as date_start_odoo, date_end as date_end_odoo, "name" as telefon_odoo,
subscriptioncode as codi_contracte_opencell, "subscription" as telefon_opencell, subscriptionstatus as subscription_status_opencell,bsiid,subscriptionid,servicecode as codi_producte_opencell,
serviceactivationdate as date_start_opencell ,serviceterminationdate as date_end_opencell, termination_date,id,useraccountcode,billing_cycle,trading_country_id,trading_language_id,billing_account_id,billingcyclecode,pais,idioma,subsbillingcycle,ebilling,buadesc
from opencell_contract_lines c
	full join odoo_contract_lines o on c.Subscriptioncode=o.code
		and coalesce(o.date_end, '99991231')>=c.Serviceactivationdate
		and o.date_start<=coalesce(c.Serviceterminationdate,'99991231');
*/

drop view if exists  validacio_factures;
create view validacio_factures as
select
case when o.code is null then 'FALTA ODOO' else 'CORRECTE' end as flag_existeix_odoo
,case when c.subscriptioncode is null then 'FALTA OPENCELL' else 'CORRECTE' end as flag_existeix_opencell
,case when coalesce(o.default_code,'xx')<>c.servicecode then 'PRODUCTE DIFERENT' else 'CORRECTE' end as flag_validacio_producte
,case when coalesce(o.date_end, '99991231')<>coalesce(c.Serviceterminationdate,'99991231') then 'DATA FI DIFERENT' else 'CORRECTE' end as flag_validacio_data_fi
,case when o.date_start<>c.Serviceactivationdate then 'DATA INICI DIFERENT' else 'CORRECTE' end as flag_validacio_data_inici
,default_code as codi_producte_odoo, code as codi_contracte_odoo, date_start as date_start_odoo, date_end as date_end_odoo, "name" as telefon_odoo,
subscriptioncode as codi_contracte_opencell, "subscription" as telefon_opencell, subscriptionstatus as subscription_status_opencell,bsiid,subscriptionid,servicecode as codi_producte_opencell,
serviceactivationdate as date_start_opencell ,serviceterminationdate as date_end_opencell, termination_date,id,useraccountcode,billing_cycle,trading_country_id,trading_language_id,billing_account_id,billingcyclecode,pais,idioma,subsbillingcycle,ebilling,buadesc
from opencell_contract_lines c
	full join odoo_contract_lines o on c.Subscriptioncode=o.code
		and coalesce(o.date_end, '99991231')>=c.Serviceactivationdate
		and o.date_start<=coalesce(c.Serviceterminationdate,'99991231');



    -- public.v_detall_factures_opencell source

CREATE OR REPLACE VIEW public.v_detall_factures_opencell
    AS SELECT d.code,
        d.invoice_date,
        d.invoice_number,
        d.amount_without_tax_invoice,
        d.amount_without_tax_rated_transaction,
        d.code_rated_transaction,
        d.quantity_rated_transaction,
        c.id,
        c.message_main_attachment_id,
        c.active,
        c.group_id,
        c.manual_currency_id,
        c.contract_template_id,
        c.user_id,
        c.recurring_next_date,
        c.date_end,
        c.payment_term_id,
        c.fiscal_position_id,
        c.invoice_partner_id,
        c.partner_id,
        c.commercial_partner_id,
        c.note,
        c.is_terminated,
        c.terminate_reason_id,
        c.terminate_comment,
        c.terminate_date,
        c.name,
        c.pricelist_id,
        c.contract_type,
        c.journal_id,
        c.company_id,
        c.create_uid,
        c.create_date,
        c.write_uid,
        c.write_date,
        c.payment_mode_id,
        c.mandate_id,
        c.service_technology_id,
        c.service_supplier_id,
        c.service_partner_id,
        c.crm_lead_line_id,
        c.mobile_contract_service_info_id,
        c.vodafone_fiber_service_contract_info_id,
        c.mm_fiber_service_contract_info_id,
        c.adsl_service_contract_info_id,
        c.date_start,
        c.phone_number,
        c.ticket_number,
        c.terminate_user_reason_id,
        c.xoln_fiber_service_contract_info_id,
        st.name AS technology_name,
        ss.name AS supplier_name,
        p.default_code AS producte,
        c.date_start AS date_start_contract,
        c.date_end AS date_end_contract
       FROM external.detall_factures_opencell d
         JOIN odoo_contract_contract c ON d.code::text = c.code::text
         JOIN odoo_service_technology st ON c.service_technology_id = st.id
         JOIN odoo_service_supplier ss ON c.service_supplier_id = ss.id
         JOIN odoo_product_product p ON p.id = c.current_tariff_product;


         -- public.v_detall_factures_opencell_month source

         CREATE OR REPLACE VIEW public.v_detall_factures_opencell_month
         AS SELECT d.data,
             o.code,
             o.amount_without_tax_rated_transaction,
             o.supplier_name,
             o.technology_name,
             o.producte
            FROM v_detall_factures_opencell o
              JOIN ( SELECT d_1.data
                    FROM data d_1
                   WHERE date_part('day'::text, d_1.data) = 1::double precision) d ON
                    date_trunc('month'::text, o.invoice_date) = d.data
                     AND date_trunc('month'::text, o.date_start_contract::timestamp with time zone) <> d.data
                     AND date_trunc('month'::text, coalesce(o.date_end_contract::timestamp with time zone, '99991231')) <> d.data;
