drop foreign table if exists keycloak_admin_event_entity;
create FOREIGN TABLE keycloak_admin_event_entity("id" varchar(36) NOT NULL
	,"admin_event_time" int8
	,"realm_id" varchar(255)
	,"operation_type" varchar(255)
	,"auth_realm_id" varchar(255)
	,"auth_client_id" varchar(255)
	,"auth_user_id" varchar(255)
	,"ip_address" varchar(255)
	,"resource_path" varchar(2550)
	,"representation" text
	,"error" varchar(255)
	,"resource_type" varchar(64))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'admin_event_entity');
drop foreign table if exists keycloak_associated_policy;
create FOREIGN TABLE keycloak_associated_policy("policy_id" varchar(36) NOT NULL
	,"associated_policy_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'associated_policy');
drop foreign table if exists keycloak_authentication_execution;
create FOREIGN TABLE keycloak_authentication_execution("id" varchar(36) NOT NULL
	,"alias" varchar(255)
	,"authenticator" varchar(36)
	,"realm_id" varchar(36)
	,"flow_id" varchar(36)
	,"requirement" int4
	,"priority" int4
	,"authenticator_flow" bool NOT NULL
	,"auth_flow_id" varchar(36)
	,"auth_config" varchar(36))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'authentication_execution');
drop foreign table if exists keycloak_authentication_flow;
create FOREIGN TABLE keycloak_authentication_flow("id" varchar(36) NOT NULL
	,"alias" varchar(255)
	,"description" varchar(255)
	,"realm_id" varchar(36)
	,"provider_id" varchar(36) NOT NULL
	,"top_level" bool NOT NULL
	,"built_in" bool NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'authentication_flow');
drop foreign table if exists keycloak_authenticator_config;
create FOREIGN TABLE keycloak_authenticator_config("id" varchar(36) NOT NULL
	,"alias" varchar(255)
	,"realm_id" varchar(36))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'authenticator_config');
drop foreign table if exists keycloak_authenticator_config_entry;
create FOREIGN TABLE keycloak_authenticator_config_entry("authenticator_id" varchar(36) NOT NULL
	,"value" text
	,"name" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'authenticator_config_entry');
drop foreign table if exists keycloak_broker_link;
create FOREIGN TABLE keycloak_broker_link("identity_provider" varchar(255) NOT NULL
	,"storage_provider_id" varchar(255)
	,"realm_id" varchar(36) NOT NULL
	,"broker_user_id" varchar(255)
	,"broker_username" varchar(255)
	,"token" text
	,"user_id" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'broker_link');
drop foreign table if exists keycloak_client;
create FOREIGN TABLE keycloak_client("id" varchar(36) NOT NULL
	,"enabled" bool NOT NULL
	,"full_scope_allowed" bool NOT NULL
	,"client_id" varchar(255)
	,"not_before" int4
	,"keycloak_client" bool NOT NULL
	,"secret" varchar(255)
	,"base_url" varchar(255)
	,"bearer_only" bool NOT NULL
	,"management_url" varchar(255)
	,"surrogate_auth_required" bool NOT NULL
	,"realm_id" varchar(36)
	,"protocol" varchar(255)
	,"node_rereg_timeout" int4
	,"frontchannel_logout" bool NOT NULL
	,"consent_required" bool NOT NULL
	,"name" varchar(255)
	,"service_accounts_enabled" bool NOT NULL
	,"client_authenticator_type" varchar(255)
	,"root_url" varchar(255)
	,"description" varchar(255)
	,"registration_token" varchar(255)
	,"standard_flow_enabled" bool NOT NULL
	,"implicit_flow_enabled" bool NOT NULL
	,"direct_access_grants_enabled" bool NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'client');
drop foreign table if exists keycloak_client_attributes;
create FOREIGN TABLE keycloak_client_attributes("client_id" varchar(36) NOT NULL
	,"value" varchar(4000)
	,"name" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'client_attributes');
drop foreign table if exists keycloak_client_auth_flow_bindings;
create FOREIGN TABLE keycloak_client_auth_flow_bindings("client_id" varchar(36) NOT NULL
	,"flow_id" varchar(36)
	,"binding_name" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'client_auth_flow_bindings');
drop foreign table if exists keycloak_client_default_roles;
create FOREIGN TABLE keycloak_client_default_roles("client_id" varchar(36) NOT NULL
	,"role_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'client_default_roles');
drop foreign table if exists keycloak_client_initial_access;
create FOREIGN TABLE keycloak_client_initial_access("id" varchar(36) NOT NULL
	,"realm_id" varchar(36) NOT NULL
	,"timestamp" int4
	,"expiration" int4
	,"count" int4
	,"remaining_count" int4)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'client_initial_access');
drop foreign table if exists keycloak_client_node_registrations;
create FOREIGN TABLE keycloak_client_node_registrations("client_id" varchar(36) NOT NULL
	,"value" int4
	,"name" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'client_node_registrations');
drop foreign table if exists keycloak_client_scope;
create FOREIGN TABLE keycloak_client_scope("id" varchar(36) NOT NULL
	,"name" varchar(255)
	,"realm_id" varchar(36)
	,"description" varchar(255)
	,"protocol" varchar(255))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'client_scope');
drop foreign table if exists keycloak_client_scope_attributes;
create FOREIGN TABLE keycloak_client_scope_attributes("scope_id" varchar(36) NOT NULL
	,"value" varchar(2048)
	,"name" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'client_scope_attributes');
drop foreign table if exists keycloak_client_scope_client;
create FOREIGN TABLE keycloak_client_scope_client("client_id" varchar(36) NOT NULL
	,"scope_id" varchar(36) NOT NULL
	,"default_scope" bool NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'client_scope_client');
drop foreign table if exists keycloak_client_scope_role_mapping;
create FOREIGN TABLE keycloak_client_scope_role_mapping("scope_id" varchar(36) NOT NULL
	,"role_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'client_scope_role_mapping');
drop foreign table if exists keycloak_client_session;
create FOREIGN TABLE keycloak_client_session("id" varchar(36) NOT NULL
	,"client_id" varchar(36)
	,"redirect_uri" varchar(255)
	,"state" varchar(255)
	,"timestamp" int4
	,"session_id" varchar(36)
	,"auth_method" varchar(255)
	,"realm_id" varchar(255)
	,"auth_user_id" varchar(36)
	,"current_action" varchar(36))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'client_session');
drop foreign table if exists keycloak_client_session_auth_status;
create FOREIGN TABLE keycloak_client_session_auth_status("authenticator" varchar(36) NOT NULL
	,"status" int4
	,"client_session" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'client_session_auth_status');
drop foreign table if exists keycloak_client_session_note;
create FOREIGN TABLE keycloak_client_session_note("name" varchar(255) NOT NULL
	,"value" varchar(255)
	,"client_session" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'client_session_note');
drop foreign table if exists keycloak_client_session_prot_mapper;
create FOREIGN TABLE keycloak_client_session_prot_mapper("protocol_mapper_id" varchar(36) NOT NULL
	,"client_session" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'client_session_prot_mapper');
drop foreign table if exists keycloak_client_session_role;
create FOREIGN TABLE keycloak_client_session_role("role_id" varchar(255) NOT NULL
	,"client_session" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'client_session_role');
drop foreign table if exists keycloak_client_user_session_note;
create FOREIGN TABLE keycloak_client_user_session_note("name" varchar(255) NOT NULL
	,"value" varchar(2048)
	,"client_session" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'client_user_session_note');
drop foreign table if exists keycloak_component;
create FOREIGN TABLE keycloak_component("id" varchar(36) NOT NULL
	,"name" varchar(255)
	,"parent_id" varchar(36)
	,"provider_id" varchar(36)
	,"provider_type" varchar(255)
	,"realm_id" varchar(36)
	,"sub_type" varchar(255))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'component');
drop foreign table if exists keycloak_component_config;
create FOREIGN TABLE keycloak_component_config("id" varchar(36) NOT NULL
	,"component_id" varchar(36) NOT NULL
	,"name" varchar(255) NOT NULL
	,"value" varchar(4000))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'component_config');
drop foreign table if exists keycloak_composite_role;
create FOREIGN TABLE keycloak_composite_role("composite" varchar(36) NOT NULL
	,"child_role" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'composite_role');
drop foreign table if exists keycloak_credential;
create FOREIGN TABLE keycloak_credential("id" varchar(36) NOT NULL
	,"device" varchar(255)
	,"hash_iterations" int4
	,"salt" bytea
	,"type" varchar(255)
	,"value" varchar(4000)
	,"user_id" varchar(36)
	,"created_date" int8
	,"counter" int4
	,"digits" int4
	,"period" int4
	,"algorithm" varchar(36))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'credential');
drop foreign table if exists keycloak_credential_attribute;
create FOREIGN TABLE keycloak_credential_attribute("id" varchar(36) NOT NULL
	,"credential_id" varchar(36) NOT NULL
	,"name" varchar(255) NOT NULL
	,"value" varchar(4000))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'credential_attribute');
drop foreign table if exists keycloak_databasechangelog;
create FOREIGN TABLE keycloak_databasechangelog("id" varchar(255) NOT NULL
	,"author" varchar(255) NOT NULL
	,"filename" varchar(255) NOT NULL
	,"dateexecuted" timestamp NOT NULL
	,"orderexecuted" int4 NOT NULL
	,"exectype" varchar(10) NOT NULL
	,"md5sum" varchar(35)
	,"description" varchar(255)
	,"comments" varchar(255)
	,"tag" varchar(255)
	,"liquibase" varchar(20)
	,"contexts" varchar(255)
	,"labels" varchar(255)
	,"deployment_id" varchar(10))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'databasechangelog');
drop foreign table if exists keycloak_databasechangeloglock;
create FOREIGN TABLE keycloak_databasechangeloglock("id" int4 NOT NULL
	,"locked" bool NOT NULL
	,"lockgranted" timestamp
	,"lockedby" varchar(255))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'databasechangeloglock');
drop foreign table if exists keycloak_default_client_scope;
create FOREIGN TABLE keycloak_default_client_scope("realm_id" varchar(36) NOT NULL
	,"scope_id" varchar(36) NOT NULL
	,"default_scope" bool NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'default_client_scope');
drop foreign table if exists keycloak_event_entity;
create FOREIGN TABLE keycloak_event_entity("id" varchar(36) NOT NULL
	,"client_id" varchar(255)
	,"details_json" varchar(2550)
	,"error" varchar(255)
	,"ip_address" varchar(255)
	,"realm_id" varchar(255)
	,"session_id" varchar(255)
	,"event_time" int8
	,"type" varchar(255)
	,"user_id" varchar(255))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'event_entity');
drop foreign table if exists keycloak_fed_credential_attribute;
create FOREIGN TABLE keycloak_fed_credential_attribute("id" varchar(36) NOT NULL
	,"credential_id" varchar(36) NOT NULL
	,"name" varchar(255) NOT NULL
	,"value" varchar(4000))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'fed_credential_attribute');
drop foreign table if exists keycloak_fed_user_attribute;
create FOREIGN TABLE keycloak_fed_user_attribute("id" varchar(36) NOT NULL
	,"name" varchar(255) NOT NULL
	,"user_id" varchar(255) NOT NULL
	,"realm_id" varchar(36) NOT NULL
	,"storage_provider_id" varchar(36)
	,"value" varchar(2024))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'fed_user_attribute');
drop foreign table if exists keycloak_fed_user_consent;
create FOREIGN TABLE keycloak_fed_user_consent("id" varchar(36) NOT NULL
	,"client_id" varchar(36)
	,"user_id" varchar(255) NOT NULL
	,"realm_id" varchar(36) NOT NULL
	,"storage_provider_id" varchar(36)
	,"created_date" int8
	,"last_updated_date" int8
	,"client_storage_provider" varchar(36)
	,"external_client_id" varchar(255))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'fed_user_consent');
drop foreign table if exists keycloak_fed_user_consent_cl_scope;
create FOREIGN TABLE keycloak_fed_user_consent_cl_scope("user_consent_id" varchar(36) NOT NULL
	,"scope_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'fed_user_consent_cl_scope');
drop foreign table if exists keycloak_fed_user_credential;
create FOREIGN TABLE keycloak_fed_user_credential("id" varchar(36) NOT NULL
	,"device" varchar(255)
	,"hash_iterations" int4
	,"salt" bytea
	,"type" varchar(255)
	,"value" varchar(255)
	,"created_date" int8
	,"counter" int4
	,"digits" int4
	,"period" int4
	,"algorithm" varchar(36)
	,"user_id" varchar(255) NOT NULL
	,"realm_id" varchar(36) NOT NULL
	,"storage_provider_id" varchar(36))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'fed_user_credential');
drop foreign table if exists keycloak_fed_user_group_membership;
create FOREIGN TABLE keycloak_fed_user_group_membership("group_id" varchar(36) NOT NULL
	,"user_id" varchar(255) NOT NULL
	,"realm_id" varchar(36) NOT NULL
	,"storage_provider_id" varchar(36))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'fed_user_group_membership');
drop foreign table if exists keycloak_fed_user_required_action;
create FOREIGN TABLE keycloak_fed_user_required_action("required_action" varchar(255) NOT NULL
	,"user_id" varchar(255) NOT NULL
	,"realm_id" varchar(36) NOT NULL
	,"storage_provider_id" varchar(36))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'fed_user_required_action');
drop foreign table if exists keycloak_fed_user_role_mapping;
create FOREIGN TABLE keycloak_fed_user_role_mapping("role_id" varchar(36) NOT NULL
	,"user_id" varchar(255) NOT NULL
	,"realm_id" varchar(36) NOT NULL
	,"storage_provider_id" varchar(36))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'fed_user_role_mapping');
drop foreign table if exists keycloak_federated_identity;
create FOREIGN TABLE keycloak_federated_identity("identity_provider" varchar(255) NOT NULL
	,"realm_id" varchar(36)
	,"federated_user_id" varchar(255)
	,"federated_username" varchar(255)
	,"token" text
	,"user_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'federated_identity');
drop foreign table if exists keycloak_federated_user;
create FOREIGN TABLE keycloak_federated_user("id" varchar(255) NOT NULL
	,"storage_provider_id" varchar(255)
	,"realm_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'federated_user');
drop foreign table if exists keycloak_group_attribute;
create FOREIGN TABLE keycloak_group_attribute("id" varchar(36) NOT NULL
	,"name" varchar(255) NOT NULL
	,"value" varchar(255)
	,"group_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'group_attribute');
drop foreign table if exists keycloak_group_role_mapping;
create FOREIGN TABLE keycloak_group_role_mapping("role_id" varchar(36) NOT NULL
	,"group_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'group_role_mapping');
drop foreign table if exists keycloak_identity_provider;
create FOREIGN TABLE keycloak_identity_provider("internal_id" varchar(36) NOT NULL
	,"enabled" bool NOT NULL
	,"provider_alias" varchar(255)
	,"provider_id" varchar(255)
	,"store_token" bool NOT NULL
	,"authenticate_by_default" bool NOT NULL
	,"realm_id" varchar(36)
	,"add_token_role" bool NOT NULL
	,"trust_email" bool NOT NULL
	,"first_broker_login_flow_id" varchar(36)
	,"post_broker_login_flow_id" varchar(36)
	,"provider_display_name" varchar(255)
	,"link_only" bool NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'identity_provider');
drop foreign table if exists keycloak_identity_provider_config;
create FOREIGN TABLE keycloak_identity_provider_config("identity_provider_id" varchar(36) NOT NULL
	,"value" text
	,"name" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'identity_provider_config');
drop foreign table if exists keycloak_identity_provider_mapper;
create FOREIGN TABLE keycloak_identity_provider_mapper("id" varchar(36) NOT NULL
	,"name" varchar(255) NOT NULL
	,"idp_alias" varchar(255) NOT NULL
	,"idp_mapper_name" varchar(255) NOT NULL
	,"realm_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'identity_provider_mapper');
drop foreign table if exists keycloak_idp_mapper_config;
create FOREIGN TABLE keycloak_idp_mapper_config("idp_mapper_id" varchar(36) NOT NULL
	,"value" text
	,"name" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'idp_mapper_config');
drop foreign table if exists keycloak_keycloak_group;
create FOREIGN TABLE keycloak_keycloak_group("id" varchar(36) NOT NULL
	,"name" varchar(255)
	,"parent_group" varchar(36)
	,"realm_id" varchar(36))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'keycloak_group');
drop foreign table if exists keycloak_keycloak_role;
create FOREIGN TABLE keycloak_keycloak_role("id" varchar(36) NOT NULL
	,"client_realm_constraint" varchar(36)
	,"client_role" bool NOT NULL
	,"description" varchar(255)
	,"name" varchar(255)
	,"realm_id" varchar(255)
	,"client" varchar(36)
	,"realm" varchar(36))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'keycloak_role');
drop foreign table if exists keycloak_migration_model;
create FOREIGN TABLE keycloak_migration_model("id" varchar(36) NOT NULL
	,"version" varchar(36))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'migration_model');
drop foreign table if exists keycloak_offline_client_session;
create FOREIGN TABLE keycloak_offline_client_session("user_session_id" varchar(36) NOT NULL
	,"client_id" varchar(36) NOT NULL
	,"offline_flag" varchar(4) NOT NULL
	,"timestamp" int4
	,"data" text
	,"client_storage_provider" varchar(36) NOT NULL
	,"external_client_id" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'offline_client_session');
drop foreign table if exists keycloak_offline_user_session;
create FOREIGN TABLE keycloak_offline_user_session("user_session_id" varchar(36) NOT NULL
	,"user_id" varchar(255) NOT NULL
	,"realm_id" varchar(36) NOT NULL
	,"last_session_refresh" int4
	,"offline_flag" varchar(4) NOT NULL
	,"data" text)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'offline_user_session');
drop foreign table if exists keycloak_policy_config;
create FOREIGN TABLE keycloak_policy_config("policy_id" varchar(36) NOT NULL
	,"name" varchar(255) NOT NULL
	,"value" text)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'policy_config');
drop foreign table if exists keycloak_protocol_mapper;
create FOREIGN TABLE keycloak_protocol_mapper("id" varchar(36) NOT NULL
	,"name" varchar(255) NOT NULL
	,"protocol" varchar(255) NOT NULL
	,"protocol_mapper_name" varchar(255) NOT NULL
	,"client_id" varchar(36)
	,"client_scope_id" varchar(36))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'protocol_mapper');
drop foreign table if exists keycloak_protocol_mapper_config;
create FOREIGN TABLE keycloak_protocol_mapper_config("protocol_mapper_id" varchar(36) NOT NULL
	,"value" text
	,"name" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'protocol_mapper_config');
drop foreign table if exists keycloak_realm;
create FOREIGN TABLE keycloak_realm("id" varchar(36) NOT NULL
	,"access_code_lifespan" int4
	,"user_action_lifespan" int4
	,"access_token_lifespan" int4
	,"account_theme" varchar(255)
	,"admin_theme" varchar(255)
	,"email_theme" varchar(255)
	,"enabled" bool NOT NULL
	,"events_enabled" bool NOT NULL
	,"events_expiration" int8
	,"login_theme" varchar(255)
	,"name" varchar(255)
	,"not_before" int4
	,"password_policy" varchar(2550)
	,"registration_allowed" bool NOT NULL
	,"remember_me" bool NOT NULL
	,"reset_password_allowed" bool NOT NULL
	,"social" bool NOT NULL
	,"ssl_required" varchar(255)
	,"sso_idle_timeout" int4
	,"sso_max_lifespan" int4
	,"update_profile_on_soc_login" bool NOT NULL
	,"verify_email" bool NOT NULL
	,"master_admin_client" varchar(36)
	,"login_lifespan" int4
	,"internationalization_enabled" bool NOT NULL
	,"default_locale" varchar(255)
	,"reg_email_as_username" bool NOT NULL
	,"admin_events_enabled" bool NOT NULL
	,"admin_events_details_enabled" bool NOT NULL
	,"edit_username_allowed" bool NOT NULL
	,"otp_policy_counter" int4
	,"otp_policy_window" int4
	,"otp_policy_period" int4
	,"otp_policy_digits" int4
	,"otp_policy_alg" varchar(36)
	,"otp_policy_type" varchar(36)
	,"browser_flow" varchar(36)
	,"registration_flow" varchar(36)
	,"direct_grant_flow" varchar(36)
	,"reset_credentials_flow" varchar(36)
	,"client_auth_flow" varchar(36)
	,"offline_session_idle_timeout" int4
	,"revoke_refresh_token" bool NOT NULL
	,"access_token_life_implicit" int4
	,"login_with_email_allowed" bool NOT NULL
	,"duplicate_emails_allowed" bool NOT NULL
	,"docker_auth_flow" varchar(36)
	,"refresh_token_max_reuse" int4
	,"allow_user_managed_access" bool NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'realm');
drop foreign table if exists keycloak_realm_attribute;
create FOREIGN TABLE keycloak_realm_attribute("name" varchar(255) NOT NULL
	,"value" varchar(255)
	,"realm_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'realm_attribute');
drop foreign table if exists keycloak_realm_default_groups;
create FOREIGN TABLE keycloak_realm_default_groups("realm_id" varchar(36) NOT NULL
	,"group_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'realm_default_groups');
drop foreign table if exists keycloak_realm_default_roles;
create FOREIGN TABLE keycloak_realm_default_roles("realm_id" varchar(36) NOT NULL
	,"role_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'realm_default_roles');
drop foreign table if exists keycloak_realm_enabled_event_types;
create FOREIGN TABLE keycloak_realm_enabled_event_types("realm_id" varchar(36) NOT NULL
	,"value" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'realm_enabled_event_types');
drop foreign table if exists keycloak_realm_events_listeners;
create FOREIGN TABLE keycloak_realm_events_listeners("realm_id" varchar(36) NOT NULL
	,"value" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'realm_events_listeners');
drop foreign table if exists keycloak_realm_required_credential;
create FOREIGN TABLE keycloak_realm_required_credential("type" varchar(255) NOT NULL
	,"form_label" varchar(255)
	,"input" bool NOT NULL
	,"secret" bool NOT NULL
	,"realm_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'realm_required_credential');
drop foreign table if exists keycloak_realm_smtp_config;
create FOREIGN TABLE keycloak_realm_smtp_config("realm_id" varchar(36) NOT NULL
	,"value" varchar(255)
	,"name" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'realm_smtp_config');
drop foreign table if exists keycloak_realm_supported_locales;
create FOREIGN TABLE keycloak_realm_supported_locales("realm_id" varchar(36) NOT NULL
	,"value" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'realm_supported_locales');
drop foreign table if exists keycloak_redirect_uris;
create FOREIGN TABLE keycloak_redirect_uris("client_id" varchar(36) NOT NULL
	,"value" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'redirect_uris');
drop foreign table if exists keycloak_required_action_config;
create FOREIGN TABLE keycloak_required_action_config("required_action_id" varchar(36) NOT NULL
	,"value" text
	,"name" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'required_action_config');
drop foreign table if exists keycloak_required_action_provider;
create FOREIGN TABLE keycloak_required_action_provider("id" varchar(36) NOT NULL
	,"alias" varchar(255)
	,"name" varchar(255)
	,"realm_id" varchar(36)
	,"enabled" bool NOT NULL
	,"default_action" bool NOT NULL
	,"provider_id" varchar(255)
	,"priority" int4)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'required_action_provider');
drop foreign table if exists keycloak_resource_attribute;
create FOREIGN TABLE keycloak_resource_attribute("id" varchar(36) NOT NULL
	,"name" varchar(255) NOT NULL
	,"value" varchar(255)
	,"resource_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'resource_attribute');
drop foreign table if exists keycloak_resource_policy;
create FOREIGN TABLE keycloak_resource_policy("resource_id" varchar(36) NOT NULL
	,"policy_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'resource_policy');
drop foreign table if exists keycloak_resource_scope;
create FOREIGN TABLE keycloak_resource_scope("resource_id" varchar(36) NOT NULL
	,"scope_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'resource_scope');
drop foreign table if exists keycloak_resource_server;
create FOREIGN TABLE keycloak_resource_server("id" varchar(36) NOT NULL
	,"allow_rs_remote_mgmt" bool NOT NULL
	,"policy_enforce_mode" varchar(15) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'resource_server');
drop foreign table if exists keycloak_resource_server_perm_ticket;
create FOREIGN TABLE keycloak_resource_server_perm_ticket("id" varchar(36) NOT NULL
	,"owner" varchar(36) NOT NULL
	,"requester" varchar(36) NOT NULL
	,"created_timestamp" int8 NOT NULL
	,"granted_timestamp" int8
	,"resource_id" varchar(36) NOT NULL
	,"scope_id" varchar(36)
	,"resource_server_id" varchar(36) NOT NULL
	,"policy_id" varchar(36))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'resource_server_perm_ticket');
drop foreign table if exists keycloak_resource_server_policy;
create FOREIGN TABLE keycloak_resource_server_policy("id" varchar(36) NOT NULL
	,"name" varchar(255) NOT NULL
	,"description" varchar(255)
	,"type" varchar(255) NOT NULL
	,"decision_strategy" varchar(20)
	,"logic" varchar(20)
	,"resource_server_id" varchar(36) NOT NULL
	,"owner" varchar(36))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'resource_server_policy');
drop foreign table if exists keycloak_resource_server_resource;
create FOREIGN TABLE keycloak_resource_server_resource("id" varchar(36) NOT NULL
	,"name" varchar(255) NOT NULL
	,"type" varchar(255)
	,"icon_uri" varchar(255)
	,"owner" varchar(36) NOT NULL
	,"resource_server_id" varchar(36) NOT NULL
	,"owner_managed_access" bool NOT NULL
	,"display_name" varchar(255))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'resource_server_resource');
drop foreign table if exists keycloak_resource_server_scope;
create FOREIGN TABLE keycloak_resource_server_scope("id" varchar(36) NOT NULL
	,"name" varchar(255) NOT NULL
	,"icon_uri" varchar(255)
	,"resource_server_id" varchar(36) NOT NULL
	,"display_name" varchar(255))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'resource_server_scope');
drop foreign table if exists keycloak_resource_uris;
create FOREIGN TABLE keycloak_resource_uris("resource_id" varchar(36) NOT NULL
	,"value" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'resource_uris');
drop foreign table if exists keycloak_scope_mapping;
create FOREIGN TABLE keycloak_scope_mapping("client_id" varchar(36) NOT NULL
	,"role_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'scope_mapping');
drop foreign table if exists keycloak_scope_policy;
create FOREIGN TABLE keycloak_scope_policy("scope_id" varchar(36) NOT NULL
	,"policy_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'scope_policy');
drop foreign table if exists keycloak_user_attribute;
create FOREIGN TABLE keycloak_user_attribute("name" varchar(255) NOT NULL
	,"value" varchar(255)
	,"user_id" varchar(36) NOT NULL
	,"id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'user_attribute');
drop foreign table if exists keycloak_user_consent;
create FOREIGN TABLE keycloak_user_consent("id" varchar(36) NOT NULL
	,"client_id" varchar(36)
	,"user_id" varchar(36) NOT NULL
	,"created_date" int8
	,"last_updated_date" int8
	,"client_storage_provider" varchar(36)
	,"external_client_id" varchar(255))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'user_consent');
drop foreign table if exists keycloak_user_consent_client_scope;
create FOREIGN TABLE keycloak_user_consent_client_scope("user_consent_id" varchar(36) NOT NULL
	,"scope_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'user_consent_client_scope');
drop foreign table if exists keycloak_user_entity;
create FOREIGN TABLE keycloak_user_entity("id" varchar(36) NOT NULL
	,"email" varchar(255)
	,"email_constraint" varchar(255)
	,"email_verified" bool NOT NULL
	,"enabled" bool NOT NULL
	,"federation_link" varchar(255)
	,"first_name" varchar(255)
	,"last_name" varchar(255)
	,"realm_id" varchar(255)
	,"username" varchar(255)
	,"created_timestamp" int8
	,"service_account_client_link" varchar(36)
	,"not_before" int4 NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'user_entity');
drop foreign table if exists keycloak_user_federation_config;
create FOREIGN TABLE keycloak_user_federation_config("user_federation_provider_id" varchar(36) NOT NULL
	,"value" varchar(255)
	,"name" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'user_federation_config');
drop foreign table if exists keycloak_user_federation_mapper;
create FOREIGN TABLE keycloak_user_federation_mapper("id" varchar(36) NOT NULL
	,"name" varchar(255) NOT NULL
	,"federation_provider_id" varchar(36) NOT NULL
	,"federation_mapper_type" varchar(255) NOT NULL
	,"realm_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'user_federation_mapper');
drop foreign table if exists keycloak_user_federation_mapper_config;
create FOREIGN TABLE keycloak_user_federation_mapper_config("user_federation_mapper_id" varchar(36) NOT NULL
	,"value" varchar(255)
	,"name" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'user_federation_mapper_config');
drop foreign table if exists keycloak_user_federation_provider;
create FOREIGN TABLE keycloak_user_federation_provider("id" varchar(36) NOT NULL
	,"changed_sync_period" int4
	,"display_name" varchar(255)
	,"full_sync_period" int4
	,"last_sync" int4
	,"priority" int4
	,"provider_name" varchar(255)
	,"realm_id" varchar(36))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'user_federation_provider');
drop foreign table if exists keycloak_user_group_membership;
create FOREIGN TABLE keycloak_user_group_membership("group_id" varchar(36) NOT NULL
	,"user_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'user_group_membership');
drop foreign table if exists keycloak_user_required_action;
create FOREIGN TABLE keycloak_user_required_action("user_id" varchar(36) NOT NULL
	,"required_action" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'user_required_action');
drop foreign table if exists keycloak_user_role_mapping;
create FOREIGN TABLE keycloak_user_role_mapping("role_id" varchar(255) NOT NULL
	,"user_id" varchar(36) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'user_role_mapping');
drop foreign table if exists keycloak_user_session;
create FOREIGN TABLE keycloak_user_session("id" varchar(36) NOT NULL
	,"auth_method" varchar(255)
	,"ip_address" varchar(255)
	,"last_session_refresh" int4
	,"login_username" varchar(255)
	,"realm_id" varchar(255)
	,"remember_me" bool NOT NULL
	,"started" int4
	,"user_id" varchar(255)
	,"user_session_state" int4
	,"broker_session_id" varchar(255)
	,"broker_user_id" varchar(255))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'user_session');
drop foreign table if exists keycloak_user_session_note;
create FOREIGN TABLE keycloak_user_session_note("user_session" varchar(36) NOT NULL
	,"name" varchar(255) NOT NULL
	,"value" varchar(2048))SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'user_session_note');
drop foreign table if exists keycloak_username_login_failure;
create FOREIGN TABLE keycloak_username_login_failure("realm_id" varchar(36) NOT NULL
	,"username" varchar(255) NOT NULL
	,"failed_login_not_before" int4
	,"last_failure" int8
	,"last_ip_failure" varchar(255)
	,"num_failures" int4)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'username_login_failure');
drop foreign table if exists keycloak_web_origins;
create FOREIGN TABLE keycloak_web_origins("client_id" varchar(36) NOT NULL
	,"value" varchar(255) NOT NULL)SERVER postgres_fdw_opencell OPTIONS (schema_name 'public', table_name 'web_origins');
